/**
 * View Account Component
 * View account's information
 */
import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {NavController} from 'ionic-angular';

import {AccountService} from '../shared/services/account.service';
import {Config} from "../../settings/config";
import {EditAccountComponent} from "../edit-account/edit-account.component";

@Component({
  selector: 'my-account',
  templateUrl: 'my-account.html'
})
export class MyAccountComponent implements OnInit {
  private DEFAULT_AVATAR_IMG = Config.DEFAULT_AVATAR_IMG;

  constructor(public account: AccountService, private title: Title, private translate: TranslateService, private navCtrl: NavController) {
    /**
     * Constructor
     */
    this.translate.get('Профиль').subscribe((res: string) => {
      this.title.setTitle(res);
    });
  }

  /**
   * Initialize Component
   * Copy account.user object to edit
   */
  ngOnInit(): void {
  }

  /** Go to Edit Account info page **/
  editAccount() {
    this.navCtrl.push(EditAccountComponent);
  }
}
