/**
 * Account Config (Constants)
 **/
import { Config } from '../settings/config';

export class AccountsConfig {
    /**
     * Accounts Common Config
     */
    public static BASIC_API_URL = Config.HOST + '/accounts/api/';
    public static BASIC_MODULE_URL = Config.APP_URL + 'accounts/';
}

export class AccountsAPIURLS {
    /**
     * Accounts API URLs Config
     */
    public static LOGIN = AccountsConfig.BASIC_API_URL + 'log-in/';
    public static LOGOUT = AccountsConfig.BASIC_API_URL + 'log-out/';
    public static GET_JSON = AccountsConfig.BASIC_API_URL + 'json';
    public static UPDATE_USER = AccountsConfig.BASIC_API_URL + 'update/';
    public static SET_EMPLOYEE_GEOLOCATION = AccountsConfig.BASIC_API_URL + 'set_geolocation/';
    public static UPDATE_USER_PHOTO = AccountsConfig.BASIC_API_URL + 'update-photo/';
    public static EMPLOYEES_LIST = AccountsConfig.BASIC_API_URL + 'employees/list';
    public static EMPLOYEES_FULL_LIST = AccountsConfig.BASIC_API_URL + 'employees/list/full';
    public static EMPLOYEE_PROFILE = AccountsConfig.BASIC_API_URL + 'employees/{username}';
    public static TOGGLE_EMPLOYEE_STATUS = AccountsConfig.BASIC_API_URL + 'employees/{username}/toggle-status';
    public static CHANGE_EMPLOYEE_ROLE = AccountsConfig.BASIC_API_URL + 'employees/{username}/change-role';
}
