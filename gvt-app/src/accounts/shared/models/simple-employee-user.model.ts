/**
 * User In List model
 * Simplified model with limited fields count
 */
import { EmployeeCard } from './employee-card.model';

export class SimpleEmployeeUser {
    /**
     * Simple employee user class
     */
    public id: number;
    public firstName: string;
    public lastName: string;
    public employeeCard: EmployeeCard;

    constructor(obj?: any) {
        if (obj) {
            this.id = obj.id;
            this.firstName = obj.first_name;
            this.lastName = obj.last_name;
            this.employeeCard = new EmployeeCard(obj.employee_card);
        }
    }
}
