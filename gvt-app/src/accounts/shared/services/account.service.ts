/**
 * Account Service
 * Load current logged in user's account
 * Manage user's account
 * Load other filtered accounts
 */


import {Observable} from 'rxjs';
import {Injectable, EventEmitter} from '@angular/core';
import {Storage} from '@ionic/storage';
import * as _ from 'lodash';

import {Geolocation} from '@ionic-native/geolocation';

import {User} from '../models/user.model';
import {EmployeeCard} from '../models/employee-card.model';
import {UserCard} from '../models/user-card.model';
import {AccountsAPIURLS} from '../../accounts.config';
import {HTTPRequestsService} from '../../../utils/services/http-requests/http-requests.service';
import {NotificationsService} from '../../../utils/services/notifications/notifications.service';

@Injectable()
export class AccountService {
  /**
   * Account Service
   */
  private isSendingGPS: boolean = false;

  constructor(private http: HTTPRequestsService, private localStorage: Storage,
              private notifications: NotificationsService, private geolocation: Geolocation) {
    this.http.authCredentialsRemoved.subscribe((removed: boolean) => {
      if (removed) {
        this.clearUser();
      }
    });
  }

  public user: User;
  public userLoaded: EventEmitter<boolean> = new EventEmitter();

  public loadUser(): void {
    /**
     * Load User using HTTP get request
     * @return: none
     */
    this.http.get(AccountsAPIURLS.GET_JSON).then(data => {
      if (data) {
        this.setUser(data);
        this.userLoaded.emit(true);
      }
    });
  }

  /** Send driver geodata **/
  public runSendGeodata(): void {
    if (this.isManagerOrDriver() && !this.isSendingGPS) {
      this.isSendingGPS = true;

      setInterval(() => {
        this.geolocation.getCurrentPosition().then((data) => {
          try {
            if (this.user) {
              let geolocationData: any = {
                latitude: data.coords.latitude,
                longitude: data.coords.longitude
              };
              this.http.post(AccountsAPIURLS.SET_EMPLOYEE_GEOLOCATION, geolocationData, false, true).then((resp: any) => {
              }, (err: any) => {
                console.log(err);
              });
            }
          } catch (e) {
            console.log(e);
          }
        }).catch((error) => {
          console.log('Error getting location', error);
        });
      }, 10000);
    }
  }

  public logIn(username: string, password: string): Promise<any> {
    /**
     * Authenticate user
     * @param username: string
     * @param password: string
     */
    return this.http.post(AccountsAPIURLS.LOGIN, {username: username, password: password}, true).then(
      (response: any) => {
        this.localStorage.set('auth_token', response.auth_token);
        return this.localStorage.set('username', response.username).then((resUsername: string) => {
          return this.localStorage.set('password', password).then((resPassword: string) => {
            this.notifications.showSuccessMessage('Авторизация прошла успешно',
              'Вы успешно вошли в приложение');
            this.loadUser();
          });
        });
      }, (errors: any) => {
        console.dir(errors);
        return errors;
      }
    );
  }

  public getUserCopy(): any {
    /**
     * Copy User object (for edit user and not modify the original object)
     */
    return _.cloneDeep(this.user);
  }

  public saveUser(user: any, photo?: any): Promise<void> {
    /**
     * Save User instance
     * Send POST request to the server
     * Update original User object (account.user)
     * @param user: copied and modified user instance (object)
     * @returns: updated user instance
     */
      // Use python property naming

    let data = {
        first_name: user.firstName,
        last_name: user.lastName,
        user_card: {
          phone_number: user.userCard.phoneNumber,
          about: user.userCard.about,
          hometown: user.userCard.hometown,
          date_of_birth: user.userCard.dateOfBirth ? user.userCard.dateOfBirth + '' : null
        }
      };

    return this.http.post(AccountsAPIURLS.UPDATE_USER, data)
      .then(data => {
        this.setUser(data);
        this.notifications.showSuccessMessage('Ваш профиль успешно обновлен',
          'Информация Вашего профиля была успешно изменена');
      });
  }

  private setUser(data: any): void {
    /**
     * Set user to the shared service
     * @param data: object like { email: 'example@email.com', firstname: 'Ann', lastname: 'Willis' }
     * @return: none
     */
    this.user = new User(
      data.email,
      data.first_name,
      data.last_name,
      data.username,
      data.is_superuser,
      data.is_staff,
      data.date_joined,
      new EmployeeCard(data.employee_card),
      new UserCard(data.user_card)
    );
  }

  public isManagerOrDriver(): boolean {
    /**
     * Check if current user is Manager or Driver
     */
    let managerRoles: string[] = ['driver', 'manager', 'admin'];

    return !!this.user && this.user.employeeCard && managerRoles.indexOf(this.user.employeeCard.role) >= 0;
  }

  public isManager(): boolean {
    /**
     * Check if current user is Manager
     */
    let managerRoles: string[] = ['manager', 'admin'];

    return !!this.user && this.user.employeeCard && managerRoles.indexOf(this.user.employeeCard.role) >= 0;
  }

  public clearUser(): void {
    /**
     * Clear user
     */
    this.user = null;
  }

  public logout(): Promise<any> {
    /**
     * Logout user. Clear storage
     */
    return this.http.post(AccountsAPIURLS.LOGOUT, {}, true).then((data) => {
      this.clearUser();
      this.notifications.showSuccessMessage('Вы успешно вышли из аккаунта',
        'Для авторизации зайдите на страницу Войти');
      this.localStorage.clear();
    }, () => {
      this.notifications.showInputErrorMessage();
    });
  }

  /**
   * Handle user loaded event. Execute 'resolve' function when user is loaded
   * @param {function} resolve: handler
   * @return: none
   **/
  public onUserLoaded(resolve: any): void {
    if (!this.user) {
      this.userLoaded.subscribe((userLoaded: boolean) => {
        if (userLoaded) {
          resolve();
        }
      });
    } else {
      resolve();
    }
  }

  public isUserLoggedIn() {
      return this.user || this.localStorage.get('');
  }
}
