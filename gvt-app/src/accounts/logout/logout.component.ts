/**
 * Log-out component
 */

import {Component} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';

import {AccountService} from '../shared/services/account.service';
import {LoginComponent} from "../login/login.component";

declare let window: any;
@Component({
  selector: 'logout-page',
  templateUrl: 'logout.html'
})
export class LogoutComponent {
  /**
   * Login component
   */

  constructor(public navCtrl: NavController, public account: AccountService, public platform: Platform) {

  }

  public logOut(): any {
    /** Logout user **/
    let _this: any = this;
    return () => {
      _this.account.logout().then(()=> {
        _this.navCtrl.setRoot(LoginComponent);

        if (window && window.cache) {
          window.cache.clear();
          window.cache.cleartemp();
        }
        _this.platform.exitApp();
      }, (errors:any) => {
        console.dir(errors);
      });

    }

  }
}



