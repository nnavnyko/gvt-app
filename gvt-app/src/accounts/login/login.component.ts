/**
 * Login component
 * Put authorization functionality here
 */
import {Component} from '@angular/core';
import {NavController, MenuController} from 'ionic-angular';
import {Storage} from '@ionic/storage';

import {AccountService} from '../shared/services/account.service';
import {AlertController} from 'ionic-angular';
import {HomePage} from "../../pages/home/home";
import {Config} from "../../settings/config";
import { TransportationRequestsListComponent } from '../../transportation/transportation-requests-list/transportation-requests-list.component';


@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginComponent {
  /**
   * Login component
   */
  private username: string;
  private password: string;
  private BIG_LOGO_IMG: string = Config.BIG_LOGO_IMG;

  constructor(public navCtrl: NavController, public account: AccountService,
              public alertCtrl: AlertController, public localStorage: Storage, private menu: MenuController) {

  }

  public logIn(): any {
    let _this: any = this;
    return () => {
      _this.account.logIn(_this.username, _this.password).then(() => {
        _this.localStorage.get('password').then((password: string) => {
          _this.localStorage.get('username').then((username: string) => {
            if (password && username) {
              _this.navCtrl.setRoot(TransportationRequestsListComponent, {listType: 'my'});
            }
          });
        });
      }, () => {
        console.log('Error occurred');
      });
    }
  }

  ionViewDidEnter() {
    // If you have more than one side menu, use the id like below
    this.menu.swipeEnable(false, 'rightMenu');
    this.menu.swipeEnable(false, 'leftMenu');
  }

  ionViewWillLeave() {
    // Don't forget to return the swipe to normal, otherwise
    // the rest of the pages won't be able to swipe to open menu
    // If you have more than one side menu, use the id like below
    // this.menu.swipeEnable(true, 'menu1');
    this.menu.swipeEnable(true, 'rightMenu');
    this.menu.swipeEnable(true, 'leftMenu');
   }
}
