/**
 * Transportation Requests Service
 */
import { Injectable, EventEmitter } from '@angular/core';
import { SimpleTransportationRequest } from '../models/simple-transportation-request.model';
import { HTTPRequestsService } from '../../../utils/services/http-requests/http-requests.service';
import { NotificationsService } from '../../../utils/services/notifications/notifications.service';
import { TransportationAPIURLS } from '../../transportation.config';
import { TransportationRequest } from '../models/transportation-request.model';
import * as _ from 'lodash';
import { AccountService } from '../../../accounts/shared/services/account.service';
import { Statistics } from '../models/statistics.model';


@Injectable()
export class TransportationRequestsService {
    /**
     * Transportation Requests Service Class
     * Load transportation requests
     * (depending on filtration my/assigned/all/details)
     */

    public statistics: Statistics;
    public statisticsLoaded: EventEmitter<boolean> = new EventEmitter();

    constructor(private http: HTTPRequestsService, private account: AccountService,
                private notifications: NotificationsService) {}

    initialize(): void {
        /**
         * Initialize Transportation Requests Service
         * Load statistics
         */
        if (!this.account.user) {
            this.account.userLoaded.subscribe(
                (userLoaded: boolean) => this.loadStatistics());
        } else {
            this.loadStatistics();
        }
    }

    public loadStatistics():void {
        /**
         * Load Transportation Requests statistics
         * Information about new/accepted/delivered/cancelled requests
         * Only managers (admin, manager or driver) can load statistics
         */
        this.statistics = new Statistics();

        if (this.account.isManagerOrDriver()) {
            this.http.get(TransportationAPIURLS.TRANSPORTATION_STATISTICS).then(data => {
                this.statistics = new Statistics(data);
                this.statisticsLoaded.emit(true);
            });
        }
    }

    loadList(url: string): Promise<any> {
        /**
         * Load list from server and generate TS-objects
         * (convert python-like TransportationRequests to TS format)
         */
        return this.http.get(url).then(data => {
            let list:SimpleTransportationRequest[] = [];

            _.each(data.results, transportationRequest => {
                list.push(new SimpleTransportationRequest(transportationRequest));
            });

            return {
                list: list,
                totalItems: data.count
            };
        });
    }

    getTransportationRequestDetails(uuid: string): Promise<any> {
        /**
         * Get TransportationRequest Details
         * @param uuid: string (TransportationRequest uuid)
         * @return: promise
         */
        return this.http.get(TransportationAPIURLS.REQUEST_DETAILS.replace('{uuid}', uuid)).then(
            data => { return new TransportationRequest(data); });
    }

    changeTransportationRequestStatus(uuid: string, type: string, data?: any): Promise<any> {
        /**
         * Change Transportation Request Status
         * @param uuid: string (TransportationRequest uuid)
         * @param type: string (type of changing status - 'assign', 'finish', 'cancel')
         * @param data: Object (additional request data)
         * @return: promise
         */
        let successMessages:any = {
            'assign': {
                title: 'Заявка успешно назначена на Вас',
                description: 'Вы успешно приняли заявку'
            },
            'cancel': {
                title: 'Заявка успешно отменена',
                description: 'Вы успешно отменили заявку'
            },
            'in_progress': {
                title: 'Заказ начат',
                description: 'Вы успешно перевели заказ в статус В процессе'
            },
            'finish': {
                title: 'Заявка успешно завершена',
                description: 'Груз доставлен'
            },
            'on_the_road': {
                title: 'Вы в пути',
                description: 'Вы направляетесь в начальную точку маршрута'
            },
            'paid': {
                title: 'Заказ оплачен',
                description: 'Заказ был успешно оплачен'
            }
        };
        data = data || {};
        data['type'] = type;

        return this.http.post(
            TransportationAPIURLS.CHANGE_REQUEST_STATUS.replace('{uuid}', uuid), data).then(
                data => {
                    this.notifications.showSuccessMessage(successMessages[type].title,
                        successMessages[type].description);
                    this.loadStatistics();
                    return new TransportationRequest(data);
                });
    }

    assignTransportationRequest(uuid: string, userID: number): Promise<any> {
        /**
         * Assign Transportation Request to user using his ID
         * @param uuid: string (TransportationRequest.uuid)
         * @param userID: number (employee.id)
         * @return: promise
         */
        return this.http.post(
            TransportationAPIURLS.ASSIGN_REQUEST.replace('{uuid}', uuid), {'assignee_id': userID}).then(
                data => {
                    this.notifications.showSuccessMessage('Сотрудник успешно назначен на заказ',
                        'Вы успешно назначили сотрудника на данный заказ.');
                    this.loadStatistics();

                    return new TransportationRequest(data);
                });
    }

    addEmployeeComment(uuid: string, employeeComment: string): Promise<any> {
        /**
         * Assign Transportation Request to user using his ID
         * @param uuid: string (TransportationRequest.uuid)
         * @param employeeComment: string
         * @return: promise
         */
        return this.http.post(
            TransportationAPIURLS.ADD_EMPLOYEE_COMMENT.replace('{uuid}', uuid), {'employee_comment': employeeComment}).then(
                data => {
                    this.notifications.showSuccessMessage('Комментарий сотрудника добавлен',
                        'Вы успешно добавили комментарий к заказу.');
                    this.loadStatistics();

                    return new TransportationRequest(data);
                });
    }

    setTransportationRequestPrice(uuid: string, carAmount?: number, portersAmount?: number,
                                  carAmountMax?: number, portersAmountMax?: number, isHourlyRate?: boolean,
                                  isCashlessPayment?: boolean): Promise<any> {
        /**
         * Assign Transportation Request to user using his ID
         * @param uuid: string (TransportationRequest.uuid)
         * @param carAmount: number or null
         * @param portersAmount: number or null
         * @param carAmountMax: number or null
         * @param portersAmountMax: number or null
         * @return: promise
         */
        return this.http.post(
            TransportationAPIURLS.SET_REQUEST_PRICE.replace('{uuid}', uuid), {
                'car_amount': carAmount,
                'porters_amount': portersAmount,
                'car_amount_max': carAmountMax,
                'porters_amount_max': portersAmountMax,
                'is_hourly_rate': isHourlyRate,
                'is_cashless_payment': isCashlessPayment,
            }).then(
                data => {
                    this.notifications.showSuccessMessage('Стоимость заказа изменена',
                        'Стоимость заказа успешно изменена.');
                    this.loadStatistics();

                    return new TransportationRequest(data);
                });
    }

    createOrEditTransportationRequest(data: any): Promise<any> {
        /**
         * Create Transportation Request and return promise.
         * The response contains uuid of the created Transportation Request
         * @param data: object
         * @return: promise
         */
        let postData: any = {
            email: data.email,
            phone: data.phone,
            is_need_porters: data.isNeedPorters,
            route_length: data.routeLength,
            porters_number: data.portersNumber,
            text: data.text,
            date: data.date,
            time: data.time
        };
        postData.points = [];
        _.each(data.points, point => {
            postData.points.push({
                sort_order: point.sortOrder,
                address: point.address,
                floor: point.floor,
                latitude: point.latitude,
                longitude: point.longitude
            })
        });
        if (!data.uuid) {
            postData.is_hourly_rate = data.isHourlyRate;
            postData.is_cashless_payment = data.isCashlessPayment;
            postData.car_amount = data.carAmount;
            postData.porters_amount = data.portersAmount;
        }
        if (data.uuid) {
            return this.http.post(TransportationAPIURLS.REQUEST_DETAILS.replace('{uuid}', data.uuid), postData).then(data => {
                this.notifications.showSuccessMessage(
                    'Заявка успешно изменена',
                    'Вы успешно изменили заявку.');
                this.loadStatistics();
                return data;
            });
        } else {
            return this.http.post(TransportationAPIURLS.CREATE_REQUEST, postData).then(data => {
                this.notifications.showSuccessMessage(
                    'Заявка успешно создана',
                    'Вы успешно оформили заявку. С Вами свяжется наш менеджер для уточнения стоимости перевозки.');
                this.loadStatistics();
                return data;
            });
        }
    }
}
