/**
 * Transportation requests list component
 * Load list and show table
 */

import {Component, OnInit, Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';
import {NavController} from 'ionic-angular';
import {NavParams} from 'ionic-angular';

import {PaginationConfig} from 'ngx-bootstrap';

import {SimpleTransportationRequest} from '../shared/models/simple-transportation-request.model';
import {TransportationRequestsService} from '../shared/services/transportation-requests.service';
import {TransportationAPIURLS, TransportationConfig} from '../transportation.config';
import {EmployeesService} from '../../accounts/shared/services/employees.service';
import {AccountService} from '../../accounts/shared/services/account.service';
import {SimpleEmployeeUser} from '../../accounts/shared/models/simple-employee-user.model';
import {TransportationConstants} from '../transportation.config';
import {CommonConstants} from '../../utils/constants/common';
import {TransportationRequestDetailsComponent} from '../transportation-request-details/transportation-request-details.component';
import {TransportationRequestUpdateComponent} from "../transportation-request-update/transportation-request-update.component";

// Constant to get list URL depending on the List Type in url parameters
const LIST_TYPE = {
  'all': TransportationAPIURLS.REQUESTS_LIST,
  'my': TransportationAPIURLS.MY_REQUESTS_LIST,
  'assigned': TransportationAPIURLS.MY_ASSIGNED_REQUESTS_LIST
};
let LIST_NAME: object = {
  'all': 'Все Заказы',
  'my': 'Мои Заказы',
  'assigned': 'Назначенные На Меня'
};

@Component({
  selector: '<transportation-requests-list></transportation-requests-list>',
  templateUrl: 'transportation-requests-list.html',
  providers: [EmployeesService, PaginationConfig]
})
export class TransportationRequestsListComponent implements OnInit {
  /**
   * Transportation Requests List Component
   */
  private trRequests: SimpleTransportationRequest[] = [];
  private totalItems: number;
  private currentPage: number;
  private filterParams: string;
  private listType: string;
  private listName: string;
  private filterStatus: string;
  private filterDateStart: string;
  private filterDateEnd: string;
  private filterSearch: string;
  private isCashlessPayment: boolean;
  private filterEmployeeID: string = "";
  private isFiltrationCollapsed: boolean = true;
  private employeesList: SimpleEmployeeUser[] = [];
  private TransportationConstants: any = TransportationConstants;
  private LIST_TYPES: any = {
    'all': 'all',
    'my': 'my',
    'assigned': 'assigned'
  };
  private requestsLoading: boolean = true;

  private statuses: any[] = CommonConstants.TRANSPORTATION_REQUESTS_STATUSES;

  constructor(private requestsService: TransportationRequestsService,
              private employeesService: EmployeesService,
              private account: AccountService, private translate: TranslateService,
              private navParams: NavParams, private navCtrl: NavController) {
    this.listType = this.navParams.get('listType') || this.LIST_TYPES.my;
    this.currentPage = 1;
    this.filterParams = '';
    this.filterStatus = '';
  }

  ngOnInit(): void {
    this.listName = LIST_NAME[this.listType];
    this.loadList(1);

    if (!this.account.user) {
      this.account.userLoaded.subscribe(
        (userLoaded: boolean) => this.loadEmployeeList());
    } else {
      this.loadEmployeeList();
    }

    _.each(this.statuses, (status: any) => {
      this.translate.get(status['display']).subscribe((res: string) => {
        status['display'] = res;
      });
    });
  }


  private loadEmployeeList(): void {
    /**
     * Load employee list if the user has permissions
     */
    if (this.account.isManager()) {
      this.employeesService.getEmployeeList().then(employeesList => {
        this.employeesList = employeesList;
      });
    }
  }
  openRequestDetails(uuid: string):void {
    this.navCtrl.setRoot(TransportationRequestDetailsComponent, {uuid: uuid});
  }
  openCreateTRRequestPage():void {
    this.navCtrl.setRoot(TransportationRequestUpdateComponent);
  }
  openEditTRRequestPage(uuid:string):void {
    this.navCtrl.setRoot(TransportationRequestUpdateComponent, {uuid: uuid});
  }


  pageChanged(infiniteScroll: any): void {
    /**
     * Handle change page
     */
    this.currentPage += 1;
    this.loadList(this.currentPage, infiniteScroll);
  }

  /**
   * Load items from next page
   * @param {number} page: next page number
   * @param {object} [infiniteScroll]: infiniteScroll event
   */
  loadList(page: number, infiniteScroll?: any): void {

    this.requestsService.loadList((LIST_TYPE[this.listType] || LIST_TYPE.my) + '?page=' + page + this.filterParams).then(data => {
      this.trRequests.push.apply(this.trRequests, data.list);
      this.totalItems = data.totalItems;
      this.requestsLoading = false;

      if (infiniteScroll) {
        infiniteScroll.complete();
      }
    });
  }

  filterByStatus(status: any): void {
    /**
     * Filter list by Created Date or Status
     * Reload list after Status Filter changed
     * @param status: string or null
     */
    this.setFilterParams(status, this.filterDateStart,
      this.filterDateEnd, this.filterSearch, this.filterEmployeeID, this.isCashlessPayment);

    this.applyFiltration();
  }

  filterByEmployeeID(employeeID: any): void {
    /**
     * Filter list by Created Date or Status
     * Reload list after Status Filter changed
     * @param employeeID: string or null
     */

    this.setFilterParams(this.filterStatus, this.filterDateStart,
      this.filterDateEnd, this.filterSearch, employeeID, this.isCashlessPayment);

    this.applyFiltration();
  }

  applyFiltration(): void {
    /**
     * Update list after filtration applied
     */
    this.currentPage = 1;
    this.trRequests = [];
    this.requestsLoading = true;

    this.loadList(1);
  }

  setFilterParams(filterStatus: string, filterDateStart: string, filterDateEnd: string,
                  filterSearch: string, filterEmployeeID: string, isCashlessPayment: boolean): void {
    /**
     * Set filtration
     */
    let filterParams = '';
    let filterArr: any[] = [
      {key: 'status', value: filterStatus},
      {key: 'date__gte', value: filterDateStart},
      {key: 'date__lte', value: filterDateEnd},
      {key: 'search', value: filterSearch},
      {key: 'assignee_id', value: filterEmployeeID},
      {key: 'is_cashless_payment', value: isCashlessPayment}
    ];
    _.each(filterArr, filterParam => {
      if (filterParam.value && filterParam.value !== '') {
        filterParams += '&' + filterParam.key + '=' + filterParam.value;
      }
    });
    this.filterParams = filterParams;
  }

  filterByDate(filterDateStart: string, filterDateEnd: string): void {
    /**
     * Filter by date: from start date to end date
     * Apply filtration by 'created' property
     * @param newDate: moment value
     * @param dateType: string (date key, start or end)
     */
    filterDateStart = filterDateStart || null;
    filterDateEnd = filterDateEnd || null;
    this.setFilterParams(this.filterStatus, filterDateStart,
      filterDateEnd, this.filterSearch, this.filterEmployeeID, this.isCashlessPayment);

    this.applyFiltration();
  }

  filterBySearch($event?: string): void {
    /**
     * Filter by search string
     * Search in phone and email fields
     * @param $event: changed search input value (optional)
     */
    this.setFilterParams(this.filterStatus, this.filterDateStart,
      this.filterDateEnd, $event, this.filterEmployeeID, this.isCashlessPayment);

    this.applyFiltration();
  }


  filterByCashlessPayment(isCashlessPayment: boolean): void {
      /**
       * Filter By Cashless Payment
       * If cashless payment checkbox inactive, then show all requests
       */
      this.setFilterParams(this.filterStatus, this.filterDateStart,
          this.filterDateEnd, this.filterSearch, this.filterEmployeeID, isCashlessPayment);

      this.applyFiltration();
  }

  /**
   * Open Transportation Requests list page
   * @param {string} listType: 'my', 'assigned' or 'all'
   */
  openTrListPage(listType:string): void {
    this.navCtrl.setRoot(TransportationRequestsListComponent, {listType: listType});
  }
}
