import {Component, Input} from "@angular/core";
import {TransportationRequest} from "../../shared/models/transportation-request.model";

@Component({
  selector: '<map-step></map-step>',
  templateUrl: 'map-step.html'
})
export class MapStepComponent {
  @Input() trRequest: TransportationRequest;
}
