/**
 * Create Transportation Request Component
 */
import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import * as _ from 'lodash';
import {NavParams, NavController, MenuController} from 'ionic-angular';

import {TransportationRequestsService} from '../shared/services/transportation-requests.service';
import {TransportationRequest} from '../shared/models/transportation-request.model';
import {TransportationLocation} from '../shared/models/transportation-location.model';
import {AccountService} from '../../accounts/shared/services/account.service';
import {TransportationRequestDetailsComponent} from '../transportation-request-details/transportation-request-details.component';
import {CommonConstants} from "../../utils/constants/common";


@Component({
  selector: '<transportation-request-update></transportation-request-update>',
  templateUrl: 'tr-request-edit.html'
})
export class TransportationRequestUpdateComponent implements OnInit, OnDestroy {
  /**
   * Create Transportation Request Component Class
   */

  private trRequest: TransportationRequest;
  private portersAmountInitialized: boolean = false;
  private phoneMask: any[] = CommonConstants.PHONE_MASK;
  private timeMask: any[] = CommonConstants.TIME_MASK;
  private maxDate: number;
  private portersNumbers: any[] = [
    {'value': 0, display: 'Без грузчиков'},
    {'value': 1, display: '1 грузчик'},
    {'value': 2, display: '2 грузчика'},
    {'value': 3, display: '3 грузчика'},
    {'value': 4, display: '4 грузчика'},
    {'value': 5, display: '5 грузчиков'},
    {'value': 6, display: '6 грузчиков'},
    {'value': 7, display: '7 грузчиков'},
    {'value': 8, display: '8 грузчиков'}
  ];

  private step: any;
  private stepCondition: any;
  private stepDefaultCondition: any;
  private currentStep: any;

  @ViewChild('submitBtn') submitBtn: any;
  @ViewChild('trRequestForm') trRequestForm: any;

  constructor(private account: AccountService, private requestsService: TransportationRequestsService,
              private params: NavParams, private navCtrl: NavController,
              private translate: TranslateService, private menu: MenuController) {
    this.step = 1;//The value of the first step, always 1
    this.stepCondition = false;//Set to true if you don't need condition in every step
    this.stepDefaultCondition = this.stepCondition;//Save the default condition for every step
  }

  ngOnInit(): void {
    /**
     * Initialize Transportation Request
     * Predefine email and phone using current session user information
     */

    this.account.onUserLoaded(() => {
      this.setInitialData();
    });
    _.each(this.portersNumbers, (portersNumber: any) => {
      this.translate.get(portersNumber['display']).subscribe((res: string) => {
        portersNumber['display'] = res;
      });
    });
    this.menu.swipeEnable(false, 'leftMenu');
    this.menu.swipeEnable(false, 'rightMenu');
  }

  portersAmountChanged(value: number): void {
    /**
     * Set isNeedPorters to true if porters Amount changed
     * @param value: number or undefined
     */
    this.trRequest.portersAmount = value;

    if (value && !this.portersAmountInitialized) {
      this.portersAmountInitialized = true;
      this.trRequest.isNeedPorters = true;
      this.trRequest.portersNumber = 2;
    }
  }

  setInitialData(): void {
    /**
     * Set initial Transportation Request data
     * Set phone and user email, if the user is not an employee
     */
    this.toggleWizardStep();
    let today = new Date();

    this.maxDate = today.getFullYear() + 2;

    let uuid: string = this.params.get('uuid');
    if (uuid) {
      this.requestsService.getTransportationRequestDetails(uuid).then(data => {
        this.trRequest = data;
        if (!this.trRequest.isNeedPorters) {
          this.trRequest.portersNumber = 0;
        }
      });
    } else {
      let trRequest: TransportationRequest = new TransportationRequest();
      trRequest.portersNumber = 0;
      trRequest.points = [new TransportationLocation({'sort_order': 1}),
        new TransportationLocation({'sort_order': 2})];
      if (!this.account.isManagerOrDriver()) {
        trRequest.phone = this.account.user.userCard.phoneNumber;
        trRequest.email = this.account.user.email;
      }
      let date = {
        'year': today.getFullYear(),
        'month': (today.getMonth() + 1),
        'date': today.getDate()
      };
      _.each(['month', 'date'], (key: string) => {
        if ((date[key] + '').length === 1) {
          date[key] = '0' + date[key];
        }
      });

      trRequest.date = [date['year'], date['month'], date['date']].join('-');

      this.trRequest = trRequest;
    }
  }

  submitForm(): any {
    /**
     * Create Transportation Request form
     * @returns: function
     */
    return () => {
      // Convert date and time to strings
      this.trRequest.date = this.trRequest.date ? this.trRequest.date + '' : null;
      this.trRequest.time = this.trRequest.time ? this.trRequest.time + '' : null;
      this.trRequest.portersNumber = !!this.trRequest.portersNumber ? this.trRequest.portersNumber : null;
      if (this.trRequest.uuid) {
        this.requestsService.createOrEditTransportationRequest(this.trRequest)
          .then(data => {
            this.navCtrl.setRoot(TransportationRequestDetailsComponent, {uuid: data.uuid});
          }, err => {
            console.dir(err);
          });
      } else {
        this.requestsService.createOrEditTransportationRequest(this.trRequest)
          .then(data => {
            this.navCtrl.setRoot(TransportationRequestDetailsComponent, {uuid: data.uuid});
          }, err => {
            console.dir(err);
          });
      }
    };
  }

  isAddressesValid(): boolean {
    /**
     * Check if addresses valid
     * All addresses must be filled
     */
    return this.trRequest && this.trRequest.points && this.trRequest.points.filter(point => !!point.address).length === this.trRequest.points.length;
  }

  cancelEdit(): void {
    /**
     * Cancel edit and go back to Request Details
     */
    this.navCtrl.setRoot(TransportationRequestDetailsComponent, {uuid: this.trRequest.uuid});
  }

  /** Handle PortersNumber changed. IF Porters number is 0 - no need porters **/
  portersNumberChanged(): void {
    if (!this.trRequest.portersAmount) {
      this.trRequest.isNeedPorters = false;
    } else {
      this.trRequest.isNeedPorters = true;
    }
  }


  /** Check if the employee can set price fields
   * The user can't set price fields if he is not an employee and if request already created **/
  canSetPriceFields(): boolean {
    return this.account.isManagerOrDriver() && (!this.trRequest || !this.trRequest.uuid);
  }

  toggleWizardStep(): void {
    this.stepCondition = !this.stepCondition;
  }

  ngOnDestroy(): void {
    this.menu.swipeEnable(true, 'leftMenu');
    this.menu.swipeEnable(true, 'rightMenu');
  }

  /**
   * Submit TransportationRequest form
   */
  onWizardFinish(): void {
    if (this.trRequestForm.form && this.trRequestForm.form.valid && this.isAddressesValid()) {
        this.submitBtn.showConfirmation();
    }
  }

}
