/**
 * Transportation Request Details Component
 * Show Transportation Request's detailed information
 */

import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {ModalDirective} from 'ngx-bootstrap';
import {$WebSocket} from 'angular2-websocket/angular2-websocket';
import {ModalController, NavController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {NavParams} from 'ionic-angular';
import * as _ from 'lodash';
import {LaunchNavigator} from '@ionic-native/launch-navigator';
import { Insomnia } from '@ionic-native/insomnia';

import {TransportationRequestsService} from '../shared/services/transportation-requests.service';
import {TransportationRequest} from '../shared/models/transportation-request.model';
import {AccountService} from '../../accounts/shared/services/account.service';
import {EmployeesService} from '../../accounts/shared/services/employees.service';
import {SimpleEmployeeUser} from '../../accounts/shared/models/simple-employee-user.model';
import {Config} from '../../settings/config';
import {NotificationsService} from '../../utils/services/notifications/notifications.service';
import {TransportationConstants} from '../transportation.config';
import {GVTWebSocketConfig} from '../../utils/services/websocket/websocket.config';
import {WebsocketUtil} from '../../utils/services/websocket/websocket.util';
import {ChangePriceComponent} from './modals/change-price/change-price.modal';
import {AssignEmployeeComponent} from './modals/assign-employee/assign-employee.modal';
import {FinishTransportationComponent} from './modals/finish-transportation/finish-transportation.modal';
import {TransportationRequestUpdateComponent} from '../transportation-request-update/transportation-request-update.component';
import {ActionsService} from '../../discounts/shared/services/actions.service';
import {Action} from '../../discounts/shared/models/action.model';

@Component({
  selector: '<transportation-request-details></transportation-request-details>',
  templateUrl: 'transportation-request-details.html',
  providers: [EmployeesService, ActionsService, Insomnia]
})
export class TransportationRequestDetailsComponent implements OnInit, OnDestroy {
  /**
   * Transportation Request Details Component class
   * Load Transportation Request Details from server using Service
   */
  private trRequest: TransportationRequest;
  private trRequestUpdating: boolean;
  private amount: number;
  private actionID: any;
  private employeeComment: string;
  private isEmployeeCommentEdit: boolean = false;
  @ViewChild('assignRequestModalInstance') public assignRequestModalInstance: ModalDirective;
  @ViewChild('completeRequestModalInstance') public completeRequestModalInstance: ModalDirective;
  private employeesList: SimpleEmployeeUser[] = [];
  private actionsDiscountsList: Action[];
  private finishedStatuses: string[] = ['cancelled', 'paid'];
  private TransportationConstants: any = TransportationConstants;

  private ws: $WebSocket;

  constructor(private navParams: NavParams,
              private requestsService: TransportationRequestsService,
              public account: AccountService,
              private employeesService: EmployeesService, private notifications: NotificationsService,
              private translate: TranslateService, private localStorage: Storage,
              private wsUtil: WebsocketUtil, private navCtrl: NavController,
              private modalCtrl: ModalController, private actionsService: ActionsService,
              private launchNavigator: LaunchNavigator, private insomnia: Insomnia
  ) {
  }

  /**
   * Initialize Component
   * Load TransportationRequest,
   */
  ngOnInit(): void {
    let uuid = this.navParams.get('uuid');
    this.insomnia.keepAwake();
    this.requestsService.getTransportationRequestDetails(uuid).then(data => {
      this.trRequest = data;
      this.amount = this.trRequest.amount;
      this.actionID = this.trRequest.actionID;
      return this.localStorage.get('auth_token').then((authToken: string) => {
        let params: string = this.wsUtil.getWSParams(authToken);

        this.ws = new $WebSocket(
          Config.getWSHost() + '/ws/transportation_requests_' + this.trRequest.uuid + params,
          null,
          new GVTWebSocketConfig()
        );
        let $this = this;
        this.ws.onMessage(
          (msg: any) => {
            let messages: object = {
              'tr_changed': {
                'title': 'Заказ изменен',
                'description': 'Заказ был изменен сотрудником'
              },
              'price_changed': {
                'title': 'Цена изменена',
                'description': 'Цена заказа была изменена'
              },
              'delivered': {
                'title': 'Заказ доставлен',
                'description': 'Заказ был доставлен'
              },
              'cancelled': {
                'title': 'Заказ отменен',
                'description': 'Заказ был отменен'
              },
              'default': {
                'title': 'Заказ изменен',
                'description': 'Заказ был изменен сотрудником'
              }
            };
            $this.requestsService.getTransportationRequestDetails(this.trRequest.uuid).then(function (data) {
              $this.trRequest = data;
              let message = messages[msg.data] || messages['default'];
              $this.notifications.showInfoMessage(message.title, message.description, 10000);
            });
          },
          {autoApply: false}
        );
      });

    });
    this.account.onUserLoaded(() => {
      this.loadEmployeeList();
      this.loadActionsDiscountsList();
    });
  }

  /**
   * Load actions discounts list if user has permissions
   */
  private loadActionsDiscountsList(): void {
    if (this.account.isManagerOrDriver()) {
      this.actionsService.getActionsList().then((data: any) => {
        this.actionsDiscountsList = data;
      });
    }
  }

  /**
   * Load employee list if the user has permissions
   */
  private loadEmployeeList(): void {
    if (this.account.isManagerOrDriver()) {
      this.employeesService.getEmployeeList().then(employeesList => {
        this.employeesList = employeesList;
      });
    }
  }

  openEditTRRequestPage(): void {
    this.navCtrl.setRoot(TransportationRequestUpdateComponent, {uuid: this.trRequest.uuid});
  }

  /**
   * Change Transportation Request Status
   * @param type: string (one of ['assign', 'cancel', 'finish'])
   * @param {boolean} [immediately]: true if need to execute immediately
   * @return: function
   */
  changeStatus(type: string, immediately?: boolean): any {
    let func: any = () => {
      this.trRequestUpdating = true;

      this.requestsService.changeTransportationRequestStatus(
        this.trRequest.uuid, type).then(data => {
        this.trRequest = data;
        this.trRequestUpdating = false;
      }, error => {
        this.trRequestUpdating = false;
      });
    };

    if (!immediately) {
      return func;
    } else {
      func();
    }
  }

  /**
   * Show Complet Transportation Modal
   */
  showCompleteTransportationRequestModal(): void {
    let modal = this.modalCtrl.create(FinishTransportationComponent, {
      trRequest: this.trRequest,
      employeesList: this.employeesList
    });

    modal.onDidDismiss(data => {
      if (data) {
        this.trRequest = data;
      }
    });

    modal.present();
  }

  /**
   * Assign user if the account.user has permissions
   * @return: function
   */
  showAssignEmployeeModal(): any {
    let modal = this.modalCtrl.create(AssignEmployeeComponent, {
      trRequest: this.trRequest,
      employeesList: this.employeesList
    });

    modal.onDidDismiss(data => {
      if (data) {
        this.trRequest = data;
      }
    });

    modal.present();
  }

  /**
   * Set cat and porters amounts and show Modal
   */
  showSetPriceModal(): void {
    let modal = this.modalCtrl.create(ChangePriceComponent, {trRequest: this.trRequest});

    modal.onDidDismiss(data => {
      if (data) {
        this.trRequest = data;
      }
    });

    modal.present();
  }

  /**
   * Pre-define employee comment to edit
   */
  editEmployeeComment(): void {
    this.isEmployeeCommentEdit = true;
    this.employeeComment = this.trRequest.employeeComment;

  }

  /**
   * Add employee comment
   * @return: function
   */
  addEmployeeComment(): any {
    this.trRequestUpdating = true;

    this.requestsService.addEmployeeComment(this.trRequest.uuid, this.employeeComment).then(data => {
      this.trRequest = data;
      this.trRequestUpdating = false;
      this.isEmployeeCommentEdit = false;
    }, error => {
      this.trRequestUpdating = false;
    });
  }

  /**
   * Handle Destroy component event
   * Close WebSocket connection
   */
  ngOnDestroy(): void {
    try {
      this.insomnia.allowSleepAgain();
      this.ws.close(true);
    } catch (err) {
      console.dir(err);
    }
  }


  /**
   * Apply Action Discount
   *
   * @param actionID: number or undefined
   * @param clearAction: boolean or undefined
   */
  applyActionDiscount(actionID?: number, clearAction?: boolean): any {
    return () => {
      let actionIDParam: number = actionID ? actionID : +this.trRequest.actionID;
      if (clearAction) {
        actionIDParam = null;
      }
      this.trRequest.actionID = actionIDParam;
      let selectedAction = _.find(this.actionsDiscountsList, {'id': actionIDParam});

      this.actionsService.applyActionDiscount(this.trRequest.uuid, selectedAction ? selectedAction.uuid : null)
        .then((data: any) => {
          if (data.uuid) {
            this.trRequest = data;
          }
        });
    }
  }

  /**
   * Check if the discount can be applied
   *
   * @return: boolean
   */
  canApplyDiscount(): boolean {
    return !!(this.account && this.account.isManagerOrDriver() && this.finishedStatuses.indexOf(this.trRequest.status) < 0 &&
      this.actionsDiscountsList && this.actionsDiscountsList.length && this.trRequest.fullAmount());
  }

  /**
   * Launch Navigator
   * @param {object} point: location point object
   **/
  navigateToPoint(point: any): void {
    this.launchNavigator.navigate([point.latitude, point.longitude], {
      appSelectionDialogHeader: 'Выберите навигатор',
      appSelectionCancelButton: 'Отмена'
    });
  }

}
