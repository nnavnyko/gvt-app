/**
 * Assign employee Controller
 */
import {Component} from '@angular/core';
import {ViewController, NavParams} from 'ionic-angular';

import {TransportationRequestsService} from '../../../shared/services/transportation-requests.service';
import {TransportationRequest} from '../../../shared/models/transportation-request.model';
import {SimpleEmployeeUser} from '../../../../accounts/shared/models/simple-employee-user.model';


@Component({
  selector: '<assign-employee-modal></assign-employee-modal>',
  templateUrl: 'assign-employee.modal.html'
})
export class AssignEmployeeComponent {
  assigneeID: number;
  trRequest: TransportationRequest;
  employeesList: SimpleEmployeeUser[] = [];

  constructor(
    private requestsService: TransportationRequestsService,
    private params: NavParams,
    private viewCtrl: ViewController
  ){
    this.trRequest = this.params.get('trRequest');
    this.employeesList = this.params.get('employeesList');
    this.assigneeID = this.trRequest.assignee ? this.trRequest.assignee.id : null;
  }

  assignEmployee(): any {
    /**
     * Assign user if the account.user has permissions
     * @return: function
     */
    return () => {

      this.requestsService.assignTransportationRequest(
        this.trRequest.uuid, this.assigneeID).then(data => {
        this.viewCtrl.dismiss(data);
      }, error => {
          console.log(error);
      });
    }
  }

  closeModal(): void{
      this.viewCtrl.dismiss(null, 'string');
  }

}
