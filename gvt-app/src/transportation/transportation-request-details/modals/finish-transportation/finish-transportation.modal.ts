/**
 * Finish TransportationRequest Controller
 */
import {Component} from '@angular/core';
import {ViewController, NavParams} from 'ionic-angular';

import {TransportationRequestsService} from '../../../shared/services/transportation-requests.service';
import {TransportationRequest} from '../../../shared/models/transportation-request.model';


@Component({
  selector: '<finish-transportation-modal></finish-transportation-modal>',
  templateUrl: 'finish-transportation.modal.html'
})
export class FinishTransportationComponent {
  finalCarAmount: number;
  finalPortersAmount: number;
  portersPayment: number;
  trRequest: TransportationRequest;

  constructor(
    private requestsService: TransportationRequestsService,
    private params: NavParams,
    private viewCtrl: ViewController
  ){
    this.trRequest = this.params.get('trRequest');
    this.finalCarAmount = this.trRequest.carAmountMax || this.trRequest.carAmount;
    this.finalPortersAmount = this.trRequest.portersAmountMax || this.trRequest.portersAmount;
    this.portersPayment = 0;
  }

  finishTransportationRequest(): any {
    /**
     * Assign user if the account.user has permissions
     * @return: function
     */
    return () => {
      this.requestsService.changeTransportationRequestStatus(
        this.trRequest.uuid, 'finish', {
          'final_car_amount': this.finalCarAmount,
          'final_porters_amount': this.finalPortersAmount,
          'porters_payment': this.portersPayment
        }).then(data => {
        this.viewCtrl.dismiss(data);
      }, error => {
          console.log(error);
      });
    }
  }

  closeModal(): void{
      this.viewCtrl.dismiss(null, 'string');
  }

}
