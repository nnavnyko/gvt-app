/**
 * Change Price Controller
 */
import {Component} from '@angular/core';
import {ViewController, NavParams} from 'ionic-angular';
import {TransportationRequestsService} from '../../../shared/services/transportation-requests.service';
import {TransportationRequest} from '../../../shared/models/transportation-request.model';


@Component({
  selector: '<change-price-modal></change-price-modal>',
  templateUrl: 'change-price.modal.html'
})
export class ChangePriceComponent {
  carAmount: number;
  carAmountMax: number;
  portersAmount: number;
  portersAmountMax: number;
  isHourlyRate: boolean;
  isCashlessPayment: boolean;
  trRequest: TransportationRequest;

  constructor(
    private requestsService: TransportationRequestsService,
    private params: NavParams,
    private viewCtrl: ViewController
  ){
    this.trRequest = this.params.get('trRequest');
    this.carAmount = this.trRequest.carAmount;
    this.carAmountMax = this.trRequest.carAmountMax;
    this.portersAmount = this.trRequest.portersAmount;
    this.portersAmountMax = this.trRequest.portersAmountMax;
    this.isHourlyRate = this.trRequest.isHourlyRate;
    this.isCashlessPayment = this.trRequest.isCashlessPayment;
  }

  setTransportationRequestPrice(): any {
    /**
     * Assign user if the account.user has permissions
     * @return: function
     */
    return () => {
      this.requestsService.setTransportationRequestPrice(this.trRequest.uuid, this.carAmount,
        this.portersAmount, this.carAmountMax, this.portersAmountMax, this.isHourlyRate, this.isCashlessPayment).then(data => {
          this.viewCtrl.dismiss(data);
      }, error => { });
    }
  }

  closeModal(): void{
      this.viewCtrl.dismiss(null, 'string');
  }

}
