/**
 * MapBox route edit directive
 */

import {AfterViewInit, Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output} from "@angular/core";
import {Config} from "../../../settings/config";
import {ModalDirective} from "ngx-bootstrap";
import {Subscriber, Observable} from "rxjs";
import {TransportationRequest} from "../../../transportation/shared/models/transportation-request.model";
import {HTTPRequestsService} from "../../services/http-requests/http-requests.service";
import * as _ from "lodash";
import {TranslateService} from "@ngx-translate/core";
import {CommonUtil} from "../../services/common.util";
import {TransportationLocation} from "../../../transportation/shared/models/transportation-location.model";
import {MapPoint} from "../models/map-point.model";
import {LaunchNavigator} from "@ionic-native/launch-navigator";


declare let mapboxgl: any;
declare let turf: any;

const ROUTE_URL: string = 'https://api.mapbox.com/optimized-trips/v1/mapbox/driving/';
const ROUTE_PARAMS: string = '?overview=full&steps=true&geometries=geojson&source=first&roundtrip=false&destination=last&access_token=';

@Component({
    selector: '<mapbox-routes></mapbox-routes>',
    templateUrl: 'mapbox-routes.html',
    providers: [ModalDirective]
})
export class MapboxRoutesComponent implements OnInit, OnDestroy, AfterViewInit {

    @Input() trRequest: TransportationRequest;

    private trRequestObservable: Observable<any>;
    private trRequestObserver: Subscriber<any>;
    @Output() trRequestOutput: EventEmitter<TransportationRequest> = new EventEmitter();

    private texts: any = {
        fromText: 'Откуда',
        toText: 'Куда',
        nextText: 'Далее',
        hourString: 'час',
        minuteString: 'мин',
        driverText: 'Водитель'
    };

    private map: any;

    private addressesList: string[];
    private mapLoaded: boolean = false;

    private mapLoadedObservable: Observable<any>;
    private mapLoadedObserver: Subscriber<any>;
    @Output() mapLoadedOutput: EventEmitter<boolean> = new EventEmitter();

    private Config: Object = Config;
    private uuid: string = CommonUtil.generateUUID();
    private routePoints: any;
    private mapPoints: MapPoint[] = [];
    private dragHandler: any = null;
    private isMapDraggable: boolean = true;
    @Input() routeLength: any;
    private routeTime: string;

    /** Constructor */
    constructor(private http: HTTPRequestsService, private zone: NgZone, private translate: TranslateService,
                private launchNavigator: LaunchNavigator) {
        let $this: any = this;

        $this.trRequestObservable = new Observable((observer: Subscriber<any>) => {
            $this.trRequestObserver = observer;
        });

        $this.mapLoadedObservable = new Observable((observer: Subscriber<any>) => {
            $this.mapLoadedObserver = observer;
        });

        $this.trRequestObservable.subscribe((data: TransportationRequest) => {
            $this.trRequest = data;
            $this.trRequestOutput.emit($this.trRequest);
        });

        $this.mapLoadedObservable.subscribe((data: boolean) => {
            $this.mapLoaded = data;
            $this.mapLoadedOutput.emit($this.mapLoaded);
        });

        let keys: string[] = ['fromText', 'toText', 'nextText', 'hourString', 'minuteString', 'driverText'];
        _.each(keys, (key) => {
            $this.translate.get($this.texts[key]).subscribe((res: string) => {
                $this.texts[key] = res;
            });
        });
    }

    /** Initialize map after component's HTML is rendered */
    ngAfterViewInit(): void {
        let $this = this;

        try {
            let map = new mapboxgl.Map({
                container: 'map-' + $this.uuid,
                style: Config.MAPBOX_STYLE,
                center: [29.2223129, 52.0322082],
                zoom: 14,
                minZoom: 5.5,
                maxZoom: 18.5,
                dragRotate: false
            });
            $this.map = map;
            $this.map.touchZoomRotate.disableRotation();

            $this.routePoints = turf.featureCollection([]);
            // Create an empty GeoJSON feature collection for drop off locations

            // Create an empty GeoJSON feature collection, which will be used as the data source for the route before users add any new data
            let nothing = turf.featureCollection([]);

            $this.map.addControl(new mapboxgl.FullscreenControl());
            $this.map.addControl(new mapboxgl.ScaleControl({
                maxWidth: 80
            }));
            $this.map.addControl(new mapboxgl.NavigationControl({showCompass: false}));

            $this.map.on('load', () => {
                $this.zone.run(() => {
                    $this.initializeMap();
                    $this.mapLoadedObserver.next(true);
                    $this.mapLoadedObserver.complete();
                });

                map.setLayoutProperty('country-label-lg', 'text-field', ['get', 'name_ru']);

                $this.map.addLayer({
                    id: 'dropoffs-symbol',
                    type: 'symbol',
                    source: {
                        data: $this.routePoints,
                        type: 'geojson'
                    },
                    layout: {
                        'icon-allow-overlap': true,
                        'icon-ignore-placement': true,
                        'icon-image': 'marker-15',
                    }
                });

                $this.map.addSource('route', {
                    type: 'geojson',
                    data: nothing
                });

                $this.map.addLayer({
                    id: 'routeline-active',
                    type: 'line',
                    source: 'route',
                    layout: {
                        'line-join': 'round',
                        'line-cap': 'round'
                    },
                    paint: {
                        'line-color': '#3c3b3c',
                        'line-width': {
                            base: 2.5,
                            stops: [[12, 3], [22, 12]]
                        }
                    }
                }, 'waterway-label');

                $this.map.addLayer({
                    id: 'routearrows',
                    type: 'symbol',
                    source: 'route',
                    layout: {
                        'symbol-placement': 'line',
                        'text-field': '▶',
                        'text-size': {
                            base: 2,
                            stops: [[12, 24], [22, 60]]
                        },
                        'symbol-spacing': {
                            base: 1,
                            stops: [[12, 30], [22, 160]]
                        },
                        'text-keep-upright': false
                    },
                    paint: {
                        'text-color': '#3c3b3c',
                        'text-halo-color': 'hsl(55, 11%, 96%)',
                        'text-halo-width': 3
                    }
                }, 'waterway-label');
            });
        } catch (err) {
            console.error(err);
        }
    }

    /** Initialize component */
    ngOnInit(): void {
        this.trRequestObserver.next(this.trRequest);
    }

    initializeMap(): void {
        let $this = this;
        _.each($this.trRequest.points, (point: TransportationLocation, i: number) => {
            let mapPoint: MapPoint = $this.addMapPoint(point);
            if (i === 0) {
                mapPoint.flyTo();
            }
        });
        $this.rebuildRoute();
    }

    /**
     * Add MapPoint to map
     * @param {TransportationLocation} location: TransportationLocation point
     * @return {MapPoint}: mapPoint object
     */
    addMapPoint(location: TransportationLocation): MapPoint {
        let $this = this;
        let mapPoint: MapPoint = new MapPoint(location, $this.map);

        mapPoint.addClickListener((e: any) => {
          let coords: any[] = mapPoint.getCoordinates();
          $this.launchNavigator.navigate([coords[1], coords[0]], {
            appSelectionDialogHeader: 'Выберите навигатор',
            appSelectionCancelButton: 'Отмена'
          });
        });

        $this.mapPoints.push(mapPoint);

        return mapPoint;
    }

    /**
     * Rebuild Route on Map
     */
    rebuildRoute(): void {
        let $this = this;
        let coordinates: any[] = [];
        _.each($this.mapPoints, (point: MapPoint) => {
            let coords: any = point.getCoordinates();
            if (coords) {
                coordinates.push(coords);
            }
        });
        $this.rebuildRoutePoints();
        if ($this.routePoints.features.length < 2) {
            return;
        }

        let url: string = ROUTE_URL +
            coordinates.join(';') +
            ROUTE_PARAMS + mapboxgl.accessToken;

        $this.http.get(url).then((data: any) => {
            let routeGeoJSON = turf.featureCollection([turf.feature(data.trips[0].geometry)]);
            let nothing = turf.featureCollection([]);

            if (!data.trips[0]) {
                $this.map.getSource('route')
                    .setData(nothing);
            } else {
                $this.routeLength = data.trips[0].distance;
                $this.routeTime = $this.getRouteTime(data.trips[0].duration);
                $this.map.getSource('route')
                    .setData(routeGeoJSON);
            }

            $this.map.getSource('dropoffs-symbol')
              .setData($this.routePoints);
        });
        this.setMarkersLabels();
    }

    /** Rebuild turf.featureCollection */
    rebuildRoutePoints(): void {
        let $this = this;
        $this.routePoints = turf.featureCollection([]);

        _.each($this.trRequest.points, (location: TransportationLocation) => {
            let point: MapPoint = $this.getPointByLocation(location);
            let turfPoint: any = point.getTurfPoint();
            if (turfPoint) {
                $this.routePoints.features.push(turfPoint);
            }
        });
    }

    /** Set Markers labels */
    setMarkersLabels(): void {
        let $this = this;

        _.each(this.trRequest.points, (location: TransportationLocation, index: number) => {
            let mapPoint: MapPoint = $this.getPointByLocation(location);
            let iconLabel: string = $this.texts.fromText;

            if (index === (this.trRequest.points.length - 1)) {
                iconLabel = $this.texts.toText;
            } else if (this.trRequest.points.length > 2 && index > 0) {
                iconLabel = $this.texts.nextText;
            }

            mapPoint.setMarkerLabel(iconLabel);
        });
    }

    /**
     * Get MapPoint by TransportationLocation
     * @param {TransportationLocation} location: TransportationLocation instance
     * @return {MapPoint}: map point instance
     */
    getPointByLocation(location: TransportationLocation): MapPoint {
        return _.find(this.mapPoints, (point: MapPoint) => {
            return point.isEqualLocation(location);
        });
    }

    /**
     * Unsubscribe observable letiables
     */
    ngOnDestroy(): void {
        try {
            this.trRequestObserver.complete();
            if (this.map) {
                this.map.remove();
            }
            if (this.dragHandler) {
                document.removeEventListener('touchstart', this.dragHandler);
                document.removeEventListener('click', this.dragHandler);
            }
        } catch (e) {
            console.log('ERROR in destroy mapbox map:', e);
        }
    }



    /** Add Drag listener on document to fix drag issue on mobile*/
    addDragListener(): void {
        let $this = this;

        let onMove: any = $this.map.dragPan._onMove;
        $this.map.dragPan._onMove = (e:any) => {
            if ($this.isMapDraggable) {
                onMove(e);
            }
        };

        $this.dragHandler = (e:any) => {
            try {
                let mapEl: any = $this.map.getContainer();
                if (e && e.path && e.path.indexOf(mapEl) < 0) {
                    $this.isMapDraggable = false;
                    $this.map.dragPan.disable();
                } else {
                    $this.isMapDraggable = true;
                    $this.map.dragPan.enable();
                }
            } catch(err) {
                console.log(err);
            }
        };

        document.addEventListener('touchstart', $this.dragHandler);
        document.addEventListener('click', $this.dragHandler);
    }



    /**
     * Get route time from Yandex Maps API
     * @param {number} routeTime: number (time in seconds)
     * @return: string (route time humanized, e.g. 1h 30min)
     */
    getRouteTime(routeTime: number): string {
        let routeTimeStr = '';

        if (!routeTime) {
            return routeTimeStr;
        }
        const hourInSeconds = 3600;
        const minuteInSeconds = 60;

        let hours = (routeTime / hourInSeconds) >> 0;
        if (hours > 0) {
            routeTimeStr += hours + ' ' + this.texts.hourString;
        }

        let minutes = ((routeTime - hours * hourInSeconds) / minuteInSeconds) >> 0;
        if (minutes > 0) {
            routeTimeStr += ' ' + minutes + ' ' + this.texts.minuteString;
        }

        return routeTimeStr;
    }

}
