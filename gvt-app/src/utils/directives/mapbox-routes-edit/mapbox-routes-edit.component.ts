/**
 * MapBox route edit directive
 */

import {AfterViewInit, Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output} from "@angular/core";
import {ModalDirective} from "ngx-bootstrap";
import * as _ from "lodash";
import {TranslateService} from "@ngx-translate/core";
import {CommonUtil} from "../../services/common.util";
import {MapPoint} from "../models/map-point.model";
import {TransportationLocation} from "../../../transportation/shared/models/transportation-location.model";
import {NotificationsService} from "../../services/notifications/notifications.service";
import {Config} from "../../../settings/config";
import {HTTPRequestsService} from "../../services/http-requests/http-requests.service";
import {TransportationRequest} from "../../../transportation/shared/models/transportation-request.model";
import {AddressInputModalComponent} from "./modals/address-input.modal";
import {ModalController} from "ionic-angular";
import {Observable, Subscriber} from "rxjs";
import {Geolocation} from "@ionic-native/geolocation";


declare let mapboxgl: any;
declare let turf: any;

const ROUTE_URL: string = 'https://api.mapbox.com/optimized-trips/v1/mapbox/driving/';
const ROUTE_PARAMS: string = '?overview=full&steps=true&geometries=geojson&source=first&roundtrip=false&destination=last&access_token=';

const SEARCH_BY_COORDS_URL: string = 'https://nominatim.openstreetmap.org/search/';
const SEARCH_BY_ADDRESS_URL: string = 'https://nominatim.openstreetmap.org/search/?countrycodes=by&';
const SEARCH_ONE_PARAMS: string = 'format=json&addressdetails=1&limit=1&&accept-language=ru';
const SEARCH_AUTOCOMPLETE_PARAMS: string = 'format=json&addressdetails=1&limit=5&namedetails=1&accept-language=ru';


@Component({
  selector: '<mapbox-routes-edit></mapbox-routes-edit>',
  templateUrl: 'mapbox-routes-edit.html',
  providers: [ModalDirective]
})
export class MapboxRoutesEditComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() trRequest: TransportationRequest;

  private trRequestObservable: Observable<any>;
  private trRequestObserver: Subscriber<any>;
  @Output() trRequestOutput: EventEmitter<TransportationRequest> = new EventEmitter();

  private texts: any = {
    fromText: 'Откуда',
    toText: 'Куда',
    nextText: 'Далее'
  };

  private map: any;
  private currentLocation: TransportationLocation;
  private currentPoint: MapPoint;
  private currentAddress: string;

  private addressesList: string[];
  private mapLoaded: boolean = false;

  private mapLoadedObservable: Observable<any>;
  private mapLoadedObserver: Subscriber<any>;
  @Output() mapLoadedOutput: EventEmitter<boolean> = new EventEmitter();

  private addressesListObservable: Observable<any>;
  private addressesListObserver: Subscriber<any>;
  @Output() addressesListOutput: EventEmitter<string[]> = new EventEmitter();

  private Config: Object = Config;
  private uuid: string = CommonUtil.generateUUID();
  private routePoints: any;
  private mapPoints: MapPoint[] = [];
  private dragHandler: any = null;
  private isMapDraggable: boolean = true;

  /** Constructor */
  constructor(private http: HTTPRequestsService, private zone: NgZone, private translate: TranslateService,
              private notifications: NotificationsService, private modalCtrl: ModalController,
              private geolocation: Geolocation) {
    let $this: any = this;

    $this.addressesListObservable = new Observable((observer: Subscriber<any>) => {
      $this.addressesListObserver = observer;
    });

    $this.trRequestObservable = new Observable((observer: Subscriber<any>) => {
      $this.trRequestObserver = observer;
    });

    $this.mapLoadedObservable = new Observable((observer: Subscriber<any>) => {
      $this.mapLoadedObserver = observer;
    });

    $this.trRequestObservable.subscribe((data: TransportationRequest) => {
      $this.trRequest = data;
      $this.trRequestOutput.emit($this.trRequest);
    });

    $this.mapLoadedObservable.subscribe((data: boolean) => {
      $this.mapLoaded = data;
      $this.mapLoadedOutput.emit($this.mapLoaded);
    });

    $this.addressesListObservable.subscribe((data: string[]) => {
      $this.addressesList = data;
      $this.addressesListOutput.emit($this.addressesList);
    });

    let keys: string[] = ['fromText', 'toText', 'nextText'];
    _.each(keys, (key) => {
      $this.translate.get($this.texts[key]).subscribe((res: string) => {
        $this.texts[key] = res;
      });
    });
  }

  /** Initialize map after component's HTML is rendered */
  ngAfterViewInit(): void {
    let $this = this;

    try {
      let map = new mapboxgl.Map({
        container: 'map-' + $this.uuid,
        style: Config.MAPBOX_STYLE,
        center: [29.2223129, 52.0322082],
        zoom: 14,
        minZoom: 5.5,
        maxZoom: 18.5,
        dragRotate: false
      });
      $this.map = map;
      $this.map.touchZoomRotate.disableRotation();

      $this.routePoints = turf.featureCollection([]);
      // Create an empty GeoJSON feature collection for drop off locations

      // Create an empty GeoJSON feature collection, which will be used as the data source for the route before users add any new data
      let nothing = turf.featureCollection([]);

      $this.map.addControl(new mapboxgl.FullscreenControl());
      $this.map.addControl(new mapboxgl.ScaleControl({
        maxWidth: 80
      }));
      $this.map.addControl(new mapboxgl.NavigationControl({showCompass: false}));

      $this.map.on('load', () => {
        setTimeout(() => {
          $this.map.resize();
        }, 10);

        $this.addClickHandler();

        map.setLayoutProperty('country-label-lg', 'text-field', ['get', 'name_ru']);
        $this.mapLoadedObserver.next(true);
        $this.mapLoadedObserver.complete();

        if (!$this.trRequest.uuid) {
          this.geolocation.getCurrentPosition().then((data) => {
            try {
              map.flyTo({center: [data.coords.longitude, data.coords.latitude]});
            } catch(e) {
              console.log(e);
            }
          });
        }

        $this.map.addLayer({
          id: 'dropoffs-symbol',
          type: 'symbol',
          source: {
            data: $this.routePoints,
            type: 'geojson'
          },
          layout: {
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'icon-image': 'marker-15',
          }
        });

        $this.map.addSource('route', {
          type: 'geojson',
          data: nothing
        });

        $this.map.addLayer({
          id: 'routeline-active',
          type: 'line',
          source: 'route',
          layout: {
            'line-join': 'round',
            'line-cap': 'round'
          },
          paint: {
            'line-color': '#3c3b3c',
            'line-width': {
              base: 2.5,
              stops: [[12, 3], [22, 12]]
            }
          }
        }, 'waterway-label');

        $this.map.addLayer({
          id: 'routearrows',
          type: 'symbol',
          source: 'route',
          layout: {
            'symbol-placement': 'line',
            'text-field': '▶',
            'text-size': {
              base: 2,
              stops: [[12, 24], [22, 60]]
            },
            'symbol-spacing': {
              base: 1,
              stops: [[12, 30], [22, 160]]
            },
            'text-keep-upright': false
          },
          paint: {
            'text-color': '#3c3b3c',
            'text-halo-color': 'hsl(55, 11%, 96%)',
            'text-halo-width': 3
          }
        }, 'waterway-label');


        $this.zone.run(() => {
          $this.initializeMap();
          $this.mapLoadedObserver.next(true);
          $this.mapLoadedObserver.complete();
        });
      });
    } catch (err) {
      console.error(err);
    }
  }

  /** Add click on map Event Listener */
  private addClickHandler(): void {
    let $this = this;

    $this.map.on('click', (e: any) => {
      if ($this.currentPoint) {
        let currentPoint = $this.currentPoint;
        currentPoint.move(e.lngLat.lng, e.lngLat.lat);
        $this.searchByCoordinates(currentPoint, currentPoint.getCoordinates());
        $this.rebuildRoute();
      }
    });
  }

  /** Initialize component */
  ngOnInit(): void {
    this.addressesListObserver.next([]);
    this.trRequestObserver.next(this.trRequest);
  }

  initializeMap(): void {
    let $this = this;
    _.each($this.trRequest.points, (point: TransportationLocation) => {
      $this.addMapPoint(point);
    });
    if ($this.trRequest.uuid && $this.trRequest.points.length) {
      $this.map.flyTo({center: [$this.trRequest.points[0].longitude, $this.trRequest.points[0].latitude]});
    }
    $this.setNextCurrentPoint();
    $this.rebuildRoute();
  }

  /**
   * Add MapPoint to map
   * @param {TransportationLocation} location: TransportationLocation point
   */
  addMapPoint(location: TransportationLocation): void {
    let $this = this;
    let mapPoint: MapPoint = new MapPoint(location, $this.map, true);
    $this.mapPoints.push(mapPoint);

    mapPoint.setDragHandler(() => {
      mapPoint.setLocationCoordinates();
      mapPoint.setTurfPoint();
      $this.searchByCoordinates(mapPoint, mapPoint.getCoordinates());
      $this.rebuildRoute();
    });
  }

  addPoint(): void {
    let $this = this;
    let location: TransportationLocation = new TransportationLocation({'sort_order': this.trRequest.points.length + 1});
    $this.trRequest.points.push(location);

    $this.addMapPoint(location);
    $this.setCurrentPoint(location);
  }

  removePoint(location: TransportationLocation): void {
    if (this.trRequest.points.length === 2) {
      throw Error('Cannot be less than 2 points!');
    }
    let mapPoint: any = this.getPointByLocation(location);

    this.mapPoints.splice(this.mapPoints.indexOf(mapPoint), 1);
    mapPoint.remove();

    this.trRequest.points.splice(this.trRequest.points.indexOf(location), 1);

    // Reorder points
    _.each(this.trRequest.points, (point, index) => {
      point.sortOrder = index + 1;
    });

    this.rebuildRoute();
  }

  /**
   * Rebuild Route on Map
   */
  rebuildRoute(): void {
    let $this = this;
    let coordinates: any[] = [];
    _.each($this.mapPoints, (point: MapPoint) => {
      let coords: any = point.getCoordinates();
      if (coords) {
        coordinates.push(coords);
      }
    });
    $this.rebuildRoutePoints();
    if ($this.routePoints.features.length < 2) {
      return;
    }

    let url: string = ROUTE_URL +
      coordinates.join(';') +
      ROUTE_PARAMS + mapboxgl.accessToken;

    $this.http.get(url).then((data: any) => {

      if (!data.trips[0]) {
        let nothing = turf.featureCollection([]);
        $this.map.getSource('route')
          .setData(nothing);
      } else {
        let routeGeoJSON = turf.featureCollection([turf.feature(data.trips[0].geometry)]);
        $this.map.getSource('route')
          .setData(routeGeoJSON);

        $this.trRequest.routeLength = data.trips[0].distance;
      }

      $this.map.getSource('dropoffs-symbol')
        .setData($this.routePoints);
    });
    this.setMarkersLabels();
  }

  /** Rebuild turf.featureCollection */
  rebuildRoutePoints(): void {
    let $this = this;
    $this.routePoints = turf.featureCollection([]);

    _.each($this.trRequest.points, (location: TransportationLocation) => {
      let point: MapPoint = $this.getPointByLocation(location);
      let turfPoint: any = point.getTurfPoint();
      if (turfPoint) {
        $this.routePoints.features.push(turfPoint);
      }
    });
  }

  /**
   * Set Current Point
   * @param {TransportationLocation} location: TransportationLocation instance
   * @param {boolean} isAutocomplete: TransportationLocation instance
   */
  setCurrentPoint(location: TransportationLocation, isAutocomplete?: boolean): void {
    if (this.currentLocation === location) {
      if (!isAutocomplete) {
        this.currentLocation = null;
        this.currentPoint = null;
      }
    } else {
      this.currentPoint = this.getPointByLocation(location);
      this.currentLocation = location;
      this.currentPoint.flyTo();
    }
  }

  /**
   * Get MapPoint by TransportationLocation
   * @param {TransportationLocation} location: TransportationLocation instance
   * @return {MapPoint}: map point instance
   */
  getPointByLocation(location: TransportationLocation): MapPoint {
    return _.find(this.mapPoints, (point: MapPoint) => {
      return point.isEqualLocation(location);
    });
  }

  /**
   * Search address by Coordinates
   * @param {MapPoint} mapPoint: current Map Point obj
   * @param {number[]} coordinates: coordinates [lng, lat]
   */
  searchByCoordinates(mapPoint: MapPoint, coordinates: number[]): void {
    let $this = this;
    let coords = [coordinates[1], coordinates[0]].join(',');

    $this.http.get(SEARCH_BY_COORDS_URL + coords + '?' + SEARCH_ONE_PARAMS).then((resp: any) => {
      let respObj: any = resp && resp.length ? resp[0] : null;
      mapPoint.setAddress($this.getHumanReadableAddress(respObj, $this.currentAddress));

      $this.setNextCurrentPoint();
    });
  }

  /**
   * Set human readable address
   * @param {any} resp: Nominatim geosearch response
   * @param {string} defaultAddress: default address (optional, empty string by default)
   * @return {string}: human readable address
   */
  getHumanReadableAddress(resp: any, defaultAddress?: string): string {
    let address: string = defaultAddress || '';
    if (resp && resp.address) {
      let respAddress = resp.address;
      let addressArr: string[] = [];
      let keys = ['country', 'city', 'town', 'road', 'house_number'];

      _.each(keys, (key: string) => {
        let localCountry = ['беларусь', 'belarus'];
        if (respAddress[key] &&
          (key !== 'country' || localCountry.indexOf(respAddress[key].toLowerCase()) < 0)) {
          addressArr.push(respAddress[key]);
        }
      });

      address = addressArr.join(' ');
    }
    return address;
  }

  showAddressAutocompleteModal(event: any, location: any): void {
    /**
     * Show address autocomplete modal
     * @param event: event
     * @param point: object
     */

    let point: MapPoint = this.getPointByLocation(location);
    this.currentPoint = point;
    let currentPoint = point;
    let $this: any = this;
    event.preventDefault();
    let modal = this.modalCtrl.create(AddressInputModalComponent, {address: this.currentPoint.getAddress()});

    modal.onDidDismiss(data => {
      let address: string = (data || '').trim();
      if (!address) {
        return;
      }
      currentPoint.setAddress(address + ' ');

      $this.http.get(SEARCH_BY_ADDRESS_URL + 'q=' + address + '&' + SEARCH_ONE_PARAMS).then((resp: any) => {
        try {
          let respObj: any = resp && resp.length ? resp[0] : null;
          if (respObj) {
            let longitude: number = parseFloat(respObj.lon);
            let latitude: number = parseFloat(respObj.lat);

            currentPoint.move(longitude, latitude);
            currentPoint.flyTo();

            $this.setNextCurrentPoint();
            $this.rebuildRoute();
          } else {
            $this.translate.get('Адрес не найден').subscribe((title: string) => {
              $this.translate.get('Координаты адреса не найдены. Укажите адрес, установив точку на карте.').subscribe((message: string) => {
                $this.notifications.showInfoMessage(title, message, 5000);
              });
            });
          }
        } catch (err) {
          return;
        }
      });
    });

    modal.present();
  }

  /**
   * Find next TransportationLocation object without set coordinates or address
   */
  setNextCurrentPoint(): void {
    this.currentPoint = _.first(_.filter(this.mapPoints, (point: MapPoint) => {
      return !point.getTurfPoint() || !point.getAddress() || !point.getCoordinates();
    }));
    this.currentLocation = this.currentPoint ? this.currentPoint.getLocation() : null;
  }

  /** Set Markers labels */
  setMarkersLabels(): void {
    let $this = this;
    _.each(this.trRequest.points, (location: TransportationLocation, index: number) => {
      let mapPoint: MapPoint = $this.getPointByLocation(location);
      let iconLabel: string = $this.texts.fromText;

      if (index === (this.trRequest.points.length - 1)) {
        iconLabel = $this.texts.toText;
      } else if (this.trRequest.points.length > 2 && index > 0) {
        iconLabel = $this.texts.nextText;
      }

      mapPoint.setMarkerLabel(iconLabel);
    });
  }

  /**
   * Unsubscribe observable letiables
   */
  ngOnDestroy(): void {
    try {
      this.trRequestObserver.complete();
      if (this.map) {
        this.map.remove();
      }
      if (this.dragHandler) {
        document.removeEventListener('touchstart', this.dragHandler);
        document.removeEventListener('click', this.dragHandler);
        let mapElements = document.getElementsByClassName('gvt-map');
        if (mapElements.length) {
          mapElements[0].removeEventListener('mouseover', this.dragHandler);
        }
      }
    } catch (e) {
      console.log('ERROR in destroy mapbox map:', e);
    }
  }

}
