/**
 * Address input modal Controller
 */
import {Component, EventEmitter, OnDestroy, Output, NgZone, ViewChild, ElementRef} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';
import {Observable, Subscriber} from 'rxjs';
import {HTTPRequestsService} from "../../../services/http-requests/http-requests.service";
import * as _ from "lodash";

const SEARCH_BY_COORDS_URL: string = 'https://nominatim.openstreetmap.org/search/';
const SEARCH_BY_ADDRESS_URL: string = 'https://nominatim.openstreetmap.org/search/?countrycodes=by&';
const SEARCH_ONE_PARAMS: string = 'format=json&addressdetails=1&limit=1&&accept-language=ru';
const SEARCH_AUTOCOMPLETE_PARAMS: string = 'format=json&addressdetails=1&limit=5&namedetails=1&accept-language=ru';

@Component({
  selector: '<address-input-modal></address-input-modal>',
  templateUrl: 'address-input.modal.html'
})
export class AddressInputModalComponent implements OnDestroy {
  currentAddress: string;
  private addressesList: string[];

  private addressesListObservable: Observable<any>;
  private addressesListObserver: Subscriber<any>;
  @Output() addressesListOutput: EventEmitter<string[]> = new EventEmitter();
  @ViewChild('addressInput', {read: ElementRef}) addressInput: ElementRef;

  constructor(private params: NavParams, private zone: NgZone,
              private viewCtrl: ViewController, private http: HTTPRequestsService) {
    this.currentAddress = this.params.get('address');

    this.addressesListObservable = new Observable((observer: Subscriber<any>) => {
      this.addressesListObserver = observer;
    });

    this.addressesListObservable.subscribe((data: string[]) => {
      this.addressesList = data;
      this.addressesListOutput.emit(this.addressesList);
    });
  }


  /**
   * Build Address Autocomplete list
   * Make HTTP request to Nominatim service
   */
  buildAddressAutocompleteList(query: any): void {
    let $this: any = this;
    let searchString = query.trim();

    if (!searchString) {
      return;
    }

    let addressVariants: string[] = $this.getAddressVariants(searchString);
    if (addressVariants.length) {
      let data: string[] = [];
      let addressLookup: string = addressVariants[0];

      $this.getAddressAutocompleteItems(addressLookup, data).then(() => {
        if (addressVariants.length === 2) {
          addressLookup = addressVariants[1];

          $this.getAddressAutocompleteItems(addressLookup, data).then(() => {
            $this.zone.run(() => {
              $this.addressesListObserver.next(data);
            });
          });
        } else {
          $this.zone.run(() => {
            $this.addressesListObserver.next(data);
          });
        }
      });

    }
  }

  /**
   * Find address autocomplete
   * @param {string} addressLookup: address string
   * @param {string[]} data: autocomplete list
   * @return {Promise}
   */
  getAddressAutocompleteItems(addressLookup: string, data: string[]): Promise<any> {
    return this.http.get(SEARCH_BY_ADDRESS_URL + addressLookup + '&' + SEARCH_AUTOCOMPLETE_PARAMS).then((resp: any) => {
      try {
        if (resp) {
          _.each(resp, (respObj) => {
            let address: string = this.getHumanReadableAddress(respObj).trim();
            if (address && data.indexOf(address) < 0) {
              data.push(address);
            }
          });
        }
      } catch (err) {
        return;
      }
    });
  }

  /**
   * Get address variants for search string
   * @param {string} searchString: query to search coordinates by address
   * @return {Array[]}: list of strings
   */
  getAddressVariants(searchString: string): string[] {
    let result: string[] = [];
    searchString = searchString.replace(',', ' ');

    let searchArr: string[] = _.filter(searchString.split(' '), (str: string) => {
      return !!str.trim();
    });
    let city: string = '';
    let street: string = '';
    let streetReverse: string = '';

    if (searchArr.length) {
      city = searchArr[0].trim();

      if (searchArr.length > 1) {
        let streetArgs: string[] = searchArr.slice(1);

        street = streetArgs.join(' ');
        streetReverse = streetArgs.reverse().join(' ');

        result.push(this.buildSearchAddressQuery(city, street));
        result.push(this.buildSearchAddressQuery(city, streetReverse));
      } else {
        result.push(this.buildSearchAddressQuery(city));
      }
    }

    return result;
  }

  /**
   * build Search Address Query
   * @param {string} city: string
   * @param {string} [street]: string
   * @return {string}: query params to search coordinates by address
   */
  buildSearchAddressQuery(city: string, street?: string): string {
    let result: string[] = ['city=', encodeURIComponent(city)];
    if (street) {
      result.push.apply(result, ['&street=', encodeURIComponent(street)]);
    }

    return result.join('');
  }

  /**
   * Set human readable address
   * @param {any} resp: Nominatim geosearch response
   * @param {string} defaultAddress: default address (optional, empty string by default)
   * @return {string}: human readable address
   */
  getHumanReadableAddress(resp: any, defaultAddress?: string): string {
    let address: string = defaultAddress || '';
    if (resp && resp.address) {
      let respAddress = resp.address;
      let addressArr: string[] = [];
      let keys = ['country', 'city', 'town', 'road', 'house_number'];

      _.each(keys, (key: string) => {
        let localCountry = ['беларусь', 'belarus'];
        if (respAddress[key] &&
          (key !== 'country' || localCountry.indexOf(respAddress[key].toLowerCase()) < 0)) {
          addressArr.push(respAddress[key]);
        }
      });

      address = addressArr.join(' ');
    }
    return address;
  }

  /**
   * Set current address
   */
  setCurrentAddress(address: string): void {
    this.currentAddress = address.trim() + ' ';
    this.addressesList = [];
    this.addressInput.nativeElement.focus();
  }

  setAddressToCurrentPoint(): any {
    /**
     * Assign user if the account.user has permissions
     * @return: function
     */
    this.viewCtrl.dismiss(this.currentAddress);
  }

  closeModal(): void {
    this.viewCtrl.dismiss(null, 'string');
  }

  ngOnDestroy(): void {
    /**
     * Unsubscribe observable variables
     */
    this.addressesListObserver.complete();
  }

}
