/**
 * Loader component. Display loader element
 **/
import {Component} from "@angular/core";


@Component({
    selector: '<loader></loader>',
    templateUrl: 'loader.html',
})
export class LoaderComponent {}
