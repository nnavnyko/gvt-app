/**
 * Confirmation modal btn
 * Display confirmation modal on click on the button
 * Display button with icon
 * Disable button after click and replace icon with loading icon
 *
 * Usage:
 *
 * <confirmation-modal [btnText]="'Ok'" [confirmationSuccess]="myMethod"></confirmation-modal>
 */
import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AlertController} from 'ionic-angular';
import * as _ from 'lodash';

import {HTTPRequestsService} from '../../services/http-requests/http-requests.service';

const NO_SUCCESS_HANDLER_PROVIDED_ERROR = 'No success handler provided! ' +
    'You need to provide method name (e.g., [confirmationModal]="mySuccessHandler").';
const SUCCESS_HANDLER_IS_NOT_FUNC_ERROR = 'Success handler must be a function instance!';

@Component({
  selector: 'confirmation-modal',
  templateUrl: 'confirmation-modal.html',

})
export class ConfirmatiomModalComponent implements OnInit {
  /**
   * Confirmation Modal class
   */
  @Input('block') block: boolean;
  @Input('outline') outline: boolean;
  @Input('btnText') buttonText: string;
  @Input('btnColor') btnColor: string;
  @Input('btnClass') btnClass: string;
  @Input('iconName') iconName: string;
  @Input('disabledCondition') disabledCondition: boolean;
  @Input('confirmationBody') confirmationBody: string;
  @Input('confirmationHeader') confirmationHeader: string;
  @Input('confirmationYes') confirmationYes: string;
  @Input('confirmationNo') confirmationNo: string;
  @Input('confirmationSuccess') confirmationSuccess: Function;

  constructor(private http: HTTPRequestsService,
              private translate: TranslateService,
              private alertCtrl: AlertController){}

  ngOnInit(): void {
    if (!this.confirmationSuccess) {
        throw new Error(NO_SUCCESS_HANDLER_PROVIDED_ERROR);
    }
    if (typeof this.confirmationSuccess !== 'function') {
        throw new Error(SUCCESS_HANDLER_IS_NOT_FUNC_ERROR);
    }

    this.confirmationBody = this.confirmationBody || 'Вы хотите продолжить?';
    this.confirmationHeader = this.confirmationHeader || 'Подтверждение';
    this.confirmationYes = this.confirmationYes || 'Да';
    this.confirmationNo = this.confirmationNo || 'Нет';
    this.btnColor = this.btnColor || 'primary';

    let keys = ['confirmationBody', 'confirmationHeader', 'confirmationYes', 'confirmationNo', 'buttonText'];
    _.each(keys, (key: string) => {
        if (this[key]) {
          this.translate.get(this[key]).subscribe((res: string) => {
            this[key] = res;
          });
        }
    });
  }

  public showConfirmation(): void {
    /**
     * Show confirmation popup window
     */
    let alert = this.alertCtrl.create({
      title: this.confirmationHeader,
      subTitle: this.confirmationBody,
      buttons: [
        this.confirmationNo,
        {
          text: this.confirmationYes,
          handler: () => {
            this.confirmationSuccess();
          }
        }
      ]
    });
    alert.present();
  }

}
