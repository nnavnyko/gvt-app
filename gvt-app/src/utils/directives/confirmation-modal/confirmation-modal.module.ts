/**
 * Confirmation Modal Module
 */

import {NgModule} from '@angular/core';
import {IonicModule} from 'ionic-angular';
import {ModalModule} from 'ngx-bootstrap';
import {ConfirmatiomModalComponent} from './confirmation-modal.component';

@NgModule({
  imports: [ModalModule, IonicModule],
  declarations: [ConfirmatiomModalComponent],
  exports: [ConfirmatiomModalComponent]
})
export class ConfirmationModalModule {
}
