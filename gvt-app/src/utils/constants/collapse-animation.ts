/**
 * Collapse animations
 */

import {animate, transition, trigger, state, style} from "@angular/animations";

export class CollapseAnimation {
  public static animations: any = trigger('animatedCollapse', ([
      state('collapsed', style({ height: '0px', 'min-height': '0px', 'display': 'none' })),
      state('not-collapsed', style({ height: '*' })),
      transition('collapsed => not-collapsed', animate('300ms ease-in')),
      transition('not-collapsed => collapsed', animate('300ms ease-out'))
    ]))
}
