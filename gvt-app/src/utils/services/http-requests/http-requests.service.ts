/**
 * HTTP Requests Service
 * This service wraps HTTP requests:
 * 1) to show common growl-messages;
 * 2) to handle common events (errors, exceptions);
 * 3) to append common HTTP Headers (such as Content-Type).
 */

import {Injectable, EventEmitter} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {NotificationsService} from '../notifications/notifications.service';
import {Storage} from '@ionic/storage';
const AuthHeader = 'Authorization';

@Injectable()
export class HTTPRequestsService {
  /**
   * HTTP Requests Service Class
   */
  public sendingRequest: boolean;
  public authCredentialsRemoved: EventEmitter<boolean> = new EventEmitter();

  constructor(private http: Http, private notifications: NotificationsService,
              private localStorage: Storage) {
    /**
     * HTTP Request Service constructor
     * Set AUTH CREDENTIALS from local storage on initialized service instance
     */
  }

  public getAuthCredentials(): Promise<any> {
    /**
     * Set session ID header for auth
     */
    return this.localStorage.get('username').then((username: string) => {
      return this.localStorage.get('password').then((password: string) => {
        if (username && password) {
          return btoa(username + ':' + password);
        }
        return;
      });
    });
  }

  public get(url: string, data?: any): Promise<any> {
    /**
     * Handle GET request
     * @param url: string (e.g. '/some/url/path')
     * @param data: object or undefined/null/false (need to convert data to url parameters, e.g. '?some_arg=Value')
     * @returns: Promise
     */
    return this.getAuthCredentials().then((authCredentials) => {
      let params = (typeof data === 'object' && !!data) ? '?' : '';

      // Convert object to HTTP URL parameters
      if (typeof data === 'object' && !!data) {
        for (let key in data) {
          if (data.hasOwnProperty(key)) {
            params += key + '=' + data[key];
          }
        }
      }

      let headers = new Headers();
      headers.set(AuthHeader, 'Basic ' + authCredentials);
      return this.http.get(url + params, new RequestOptions({headers: headers})).toPromise()
      .then(response => {
        return response.json();
      }).catch(err => {
        if (err.status === 401) {
          this.localStorage.remove('username');
          this.localStorage.remove('password');
          this.authCredentialsRemoved.emit(true);
          this.notifications.showInfoMessage('Нет данных Авторизации', 'Вы не авторизованы. Вам необходимо Войти в систему');
        } else {
          this.notifications.showServerErrorMessage();
        }
        console.dir(err);
        // throw Error(err);
      });
    });
  }

  public post(url: string, data?: any, noSession?: boolean, noLogError?: boolean): Promise<any> {
    /**
     * Handle POST request
     * @param url: string (e.g. '/some/url/path')
     * @param data: object or undefined/null/false (need to convert data to url parameters, e.g. '?some_arg=Value')
     * @param noSession: boolean (if no need to pass authCredentials)
     * @param noLogError: boolean (if no need to show error)
     * @returns: Promise
     */
    return this.getAuthCredentials().then((authCredentials) => {
      let headers = new Headers({'Content-Type': 'application/json'});
      if (!noSession) {
        headers.set(AuthHeader, 'Basic ' + authCredentials);
      }
      if (!noLogError) {
        this.sendingRequest = true;
      }
      return this.http.post(url, data, new RequestOptions({headers: headers})).toPromise()
        .then(response => {
          this.sendingRequest = false;
          return response.json();
        }).catch(err => {
          this.sendingRequest = false;
          if (err.status === 401) {
            this.localStorage.remove('username');
            this.localStorage.remove('password');
            this.authCredentialsRemoved.emit(true);
          } else if (err.status === 400 && !noLogError) {
            this.notifications.showInputErrorMessage();
          }
          // If server error
          if ((!err.status || (err.status + '')[0] === '5') && !noLogError) {
            this.notifications.showServerErrorMessage();
          }
          console.dir(err);
          if (!noLogError) {
            throw Error(err);
          }
        });
    });
  }
}
