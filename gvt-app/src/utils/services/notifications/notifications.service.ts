/**
 * Notifications service
 */

import {Injectable} from '@angular/core';
import {AlertController} from 'ionic-angular';
import {ToastController} from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';


@Injectable()
export class NotificationsService {
  /**
   * Notifications Service class
   */
  constructor(private toastCtrl: ToastController,
              private translate: TranslateService, private alertCtrl: AlertController) {
  }

  public showServerErrorMessage(): void {
    /**
     * This method show growl error message
     * Use this method when Server Error occurred
     */

    let title: string = 'Произошла ошибка';
    let okButton: string = 'Ок';
    let description = 'Пожалуйста, обратитесь к администратору, отправив сообщение на gvtmozyr@gmail.com. Приносим свои извинения за причененные неудобства';

    this.translate.get(title).subscribe((resTitle: string) => {
      this.translate.get(description).subscribe((resDesc: string) => {
        this.translate.get(okButton).subscribe((resOk: string) => {
          let toast = this.toastCtrl.create({
            message: resTitle + '. ' + resDesc,
            position: 'bottom',
            cssClass: 'toast-error',
            duration: 0,
            showCloseButton: true,
            closeButtonText: resOk
          });

          toast.present();
        });
      });
    });
  }

  public showInputErrorMessage(): void {
    /**
     * This method show growl error message
     * Use this method when Server Error occurred
     */

    let title: string = 'Ошибка';
    let okButton: string = 'Ок';
    let description = 'Введены неверные данные. Пожалуйста, попробуйте еще раз';

    this.translate.get(title).subscribe((resTitle: string) => {
      this.translate.get(description).subscribe((resDesc: string) => {
        this.translate.get(okButton).subscribe((resOk: string) => {

          let alert = this.alertCtrl.create({
            title: resTitle,
            subTitle: resDesc,
            buttons: [resOk],
            cssClass: 'toast-error'
          });
          alert.present();
        });
      });
    });
  }

  public showSuccessMessage(title: string, message: string, duration?: number): void {
    /**
     * Show success message (green background)
     * @param title: title of the message (string)
     * @param message: message text (string)
     * @param duration: duration in milliseconds (number)
     */
    let okButton: string = 'Ок';
    this.translate.get(title).subscribe((resTitle: string) => {
      this.translate.get(message).subscribe((resDesc: string) => {
        this.translate.get(okButton).subscribe((resOk: string) => {
          let toast = this.toastCtrl.create({
            message: resTitle + '. ' + resDesc,
            position: 'bottom',
            duration: duration || 3000,
            cssClass: 'toast-success',
            showCloseButton: true,
            closeButtonText: resOk
          });

          toast.present();
        });
      });
    });
  }

  public showInfoMessage(title: string, message: string, duration?: number): void {
    /**
     * Show Info message (blue background)
     * @param title: title of the message (string)
     * @param message: message text (string)
     * @param duration: duration in milliseconds (number)
     */
    let okButton: string = 'Ок';
    this.translate.get(title).subscribe((resTitle: string) => {
      this.translate.get(message).subscribe((resDesc: string) => {
        this.translate.get(okButton).subscribe((resOk: string) => {
          let toast = this.toastCtrl.create({
            message: resTitle + '. ' + resDesc,
            position: 'bottom',
            duration: typeof duration === 'undefined' ? 3000 : duration,
            cssClass: 'toast-info',
            showCloseButton: true,
            closeButtonText: resOk
          });

          toast.present();
        });
      });
    });

  }
}
