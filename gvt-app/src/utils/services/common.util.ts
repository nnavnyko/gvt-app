/**
 * Common utility
 *
 * Place common shared methods here
 **/
import {Injectable} from "@angular/core";


@Injectable()
export class CommonUtil {

    /**
     * Generate Random string
     * return {string}: 40 chars
     **/
    public static generateUUID(): string {

        /**
         * Generate 4 chars of uuid
         * @return {string}: 4 chars
         **/
        function s4():string {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return [s4(), s4(), s4(), s4(), s4(), s4(), s4(), s4()].join('-');
    }
}