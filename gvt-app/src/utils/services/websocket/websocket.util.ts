/**
 * WebSocket utility
 */

import {Injectable} from '@angular/core';

@Injectable()
export class WebsocketUtil {

  getWSParams(authToken:string): string{
    /**
     * Get WS params string (?auth_token=<token>&subscribe_user)
     *
     * @param authToken: JWT string
     */
      let params: string = '?auth_token={auth_token}&subscribe-user';
      params = params.replace('{auth_token}', authToken);

      return params;
  }
}
