/**
 * Action discount model
 * Describe model fields: title, discount value and symbol ('%' or 'BYN')
 */

export class Action {
    public id: number;
    public uuid: string;
    public title: string;
    public discountValue: number;
    public discountSymbol: string;

    constructor(obj?: any) {
        if (!!obj) {
            this.id = obj.id;
            this.uuid = obj.uuid;
            this.title = obj.title;
            this.discountValue = obj.discount_value;
            this.discountSymbol = obj.discount_symbol;
        }
    }
}
