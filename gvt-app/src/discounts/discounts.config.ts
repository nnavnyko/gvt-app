/**
 * Discounts Config
 */

import { Config } from '../settings/config';

export class DiscountsConfig {
    /**
     * Transportation Module Config
     */
    public static BASIC_API_URL = Config.HOST + '/discounts/api/';
}

export class DiscountsAPIURLS {
    /**
     * Transportation API URLs config
     */
    // List URLs
    public static ACTIONS_LIST = DiscountsConfig.BASIC_API_URL + 'actions/list';
    public static APPLY_ACTION_DISCOUNT = DiscountsConfig.BASIC_API_URL + 'actions/apply/{uuid}/';
    public static CLEAR_ACTION_DISCOUNT = DiscountsConfig.BASIC_API_URL + 'actions/clear/';
}
