import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Storage} from '@ionic/storage';
import {Config} from '../settings/config';
import {$WebSocket} from 'angular2-websocket/angular2-websocket';
import {ScreenOrientation} from '@ionic-native/screen-orientation';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Diagnostic } from '@ionic-native/diagnostic';

import {HomePage} from '../pages/home/home';
import {NotificationsService} from '../utils/services/notifications/notifications.service';
import {GVTWebSocketConfig} from '../utils/services/websocket/websocket.config';
import {LoginComponent} from '../accounts/login/login.component';
import {LogoutComponent} from '../accounts/logout/logout.component';
import {AccountService} from '../accounts/shared/services/account.service'
import {WebsocketUtil} from '../utils/services/websocket/websocket.util';
import {TransportationRequestsService} from '../transportation/shared/services/transportation-requests.service';
import {TransportationRequestsListComponent} from '../transportation/transportation-requests-list/transportation-requests-list.component';
import {TransportationRequestUpdateComponent} from '../transportation/transportation-request-update/transportation-request-update.component';
import {CollapseAnimation} from "../utils/constants/collapse-animation";
import {MyAccountComponent} from "../accounts/my-account/my-account.component";
import {Geolocation} from "@ionic-native/geolocation";

@Component({
  templateUrl: 'app.html',
  animations: [
    CollapseAnimation.animations
  ],
  providers: [ScreenOrientation, BackgroundMode, Diagnostic, Geolocation]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  rootParams: any;
  trListType: string = 'my';
  DEFAULT_AVATAR_IMG = Config.DEFAULT_AVATAR_IMG;

  homePage: { title: string, component: any };
  loginPage: { title: string, component: any };
  logoutPage: { title: string, component: any };
  myAccountPage: { title: string, component: any };
  transportationListPage: { title: string, component: any };
  transportationRequestCreatePage: { title: string, component: any };
  appVersion: string = Config.APP_VERSION;

  isTransportationCollapsed: string = 'collapsed';
  private ws: $WebSocket;

  constructor(public platform: Platform, public statusBar: StatusBar,
              public splashScreen: SplashScreen, public account: AccountService,
              private localStorage: Storage, public trRequests: TransportationRequestsService,
              private notifications: NotificationsService, private wsUtil: WebsocketUtil,
              private screenOrientation: ScreenOrientation, private backgroundMode: BackgroundMode,
              private diagnostic: Diagnostic, private geolocation: Geolocation
  ) {
    this.rootParams = {listType: 'my'};
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.homePage = {title: 'General Information', component: HomePage};
    this.loginPage = {title: 'Log In', component: LoginComponent};
    this.logoutPage = {title: 'Log Out', component: LogoutComponent};
    this.myAccountPage = {title: 'MyAccount', component: MyAccountComponent};
    this.transportationListPage = {title: 'Transportation List', component: TransportationRequestsListComponent};
    this.transportationRequestCreatePage = {title: 'Create Order', component: TransportationRequestUpdateComponent};

    let $this = this;
    this.account.onUserLoaded(() => {
      trRequests.initialize();

      this.localStorage.get('auth_token').then((authToken: string) => {
        let params: string = $this.wsUtil.getWSParams(authToken);
        $this.ws = new $WebSocket(
          Config.getWSHost() + '/ws/transportation_requests_app' + params,
          null,
          new GVTWebSocketConfig()
        );

        this.ws.onMessage(
        (msg: any) => {
          let messages: object = {
            'tr_created': {
              'title': 'Создан новый заказ',
              'description': 'Была оформлена заявка'
            },
            'tr_request_assigned': {
              'title': 'На Вас назначен заказ',
              'description': 'Заявка была назначена на Вас. Просмотрите список заявок'
            }
          };
          $this.trRequests.loadStatistics();
          let message = messages[msg.data];

          if (message) {
            $this.notifications.showInfoMessage(message.title, message.description, 0);
          }
        },
        {autoApply: false}
      );
      });

    });

    this.localStorage.get('username').then((resUsername: any) => {
      this.localStorage.get('password').then((resPassword: any) => {
        if (resUsername && resPassword) {
            this.account.loadUser();

            this.account.onUserLoaded(() => {
              this.setTrListType();
              this.rootParams.listType = this.trListType;
              this.rootPage = TransportationRequestsListComponent;
            });
        } else {
          this.rootPage = LoginComponent;
        }
      });
    });
  }

  /**
   * Set Transportation Requests list view params
   */
  private setTrListType(): void {
    let listType: string = 'my';

    if (this.account.isManager()) {
      listType = 'all';
    } else if (this.account.isManagerOrDriver()) {
      listType = 'assigned';
    }

    this.trListType = listType;
  }

  /**
   * Initialize application and plugins
   **/
  initializeApp() {
    this.platform.ready().then(() => {
      if (!this.platform.is('cordova')) {
        return;
      }

      this.loadPlugin(()=> {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      });

      this.loadPlugin(()=> {
        this.geolocation.getCurrentPosition();
      });

      this.loadPlugin(()=>{
        this.backgroundMode.enable();
        this.backgroundMode.on('enable').subscribe(()=>{
            this.account.onUserLoaded(() => {
              this.account.runSendGeodata();
              this.backgroundMode.configure({
                title: 'Грузи В ТАКСИ',
                text: 'Приложение отслеживает местоположение водителя'
              });
              this.backgroundMode.setDefaults({
                title: 'Грузи В ТАКСИ',
                text: 'Приложение отслеживает местоположение водителя'
              });
            });
        });

        this.backgroundMode.on('activate').subscribe(()=>{
          this.account.onUserLoaded(() => {
            this.backgroundMode.disableWebViewOptimizations();
          });
        });
      });

      this.loadPlugin(()=>{
        this.diagnostic.isGpsLocationEnabled().then((enabled: boolean) => {
          if (!enabled) {
            this.diagnostic.switchToLocationSettings();
          }
        });
      });

      this.loadPlugin(()=>{
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT).then(() => {
          console.log(this.screenOrientation.type);
        }, (err) => console.log(err));
      });

      this.loadPlugin(()=>{
        if (window['cordova'] && window['cordova'].plugins.Keyboard) {
          // This requires installation of https://github.com/driftyco/ionic-plugin-keyboard
          // and can only affect native compiled Ionic2 apps (not webserved).
          window['cordova'].plugins.Keyboard.disableScroll(true);
        }
      });
    });
  }

  /**
   * Load plugin and execute handler
   * @param {function} func: function to execute plugin's methods
   **/
  loadPlugin(func: any): void {
    try {
      func();
    } catch (err) {
      console.log(err);
    }
  }

  toggleTransportationCollapse():void {
    this.isTransportationCollapsed = this.isTransportationCollapsed === 'collapsed' ? 'not-collapsed' : 'collapsed';
  }

  transportationCollapsed(): string {
    return this.isTransportationCollapsed;
  }

  openPage(page, params?: object) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component, params);
  }

  goToPage(page) {
    /**
     * Go to page with add page to the stack
     * @param page: object ({'title': 'Some title', 'component': <@Component_instance>})
     */
    this.nav.push(page.component);
  }
}
