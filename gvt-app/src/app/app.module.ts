import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {IonicStorageModule} from '@ionic/storage';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {Http, HttpModule} from '@angular/http';
import {CollapseModule, PaginationModule} from 'ngx-bootstrap';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {Geolocation} from '@ionic-native/geolocation';
import {LaunchNavigator} from '@ionic-native/launch-navigator';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {LoginComponent} from '../accounts/login/login.component';
import {LogoutComponent} from '../accounts/logout/logout.component';
import {EditAccountComponent} from '../accounts/edit-account/edit-account.component';
import {AccountService} from '../accounts/shared/services/account.service';
import {HTTPRequestsModule} from '../utils/services/http-requests/http-requests.module';
import {NotificationsModule} from '../utils/services/notifications/notifications.module';
import {ConfirmationModalModule} from '../utils/directives/confirmation-modal/confirmation-modal.module';
import {TransportationRequestsService} from '../transportation/shared/services/transportation-requests.service'
import {TransportationRequestsListComponent} from '../transportation/transportation-requests-list/transportation-requests-list.component';
import {TransportationRequestDetailsComponent} from '../transportation/transportation-request-details/transportation-request-details.component';
import {WebsocketUtil} from '../utils/services/websocket/websocket.util';
import {ChangePriceComponent} from '../transportation/transportation-request-details/modals/change-price/change-price.modal';
import {AssignEmployeeComponent} from '../transportation/transportation-request-details/modals/assign-employee/assign-employee.modal';
import {FinishTransportationComponent} from '../transportation/transportation-request-details/modals/finish-transportation/finish-transportation.modal';
import {TransportationRequestUpdateComponent} from '../transportation/transportation-request-update/transportation-request-update.component';
import {AddressInputModalComponent} from "../utils/directives/mapbox-routes-edit/modals/address-input.modal";
import {LoaderComponent} from "../utils/directives/loader/loader.component";
import {MyAccountComponent} from "../accounts/my-account/my-account.component";
import {MapStepComponent} from "../transportation/transportation-request-update/wizard-form/map-step.component";
import {MapboxRoutesComponent} from "../utils/directives/mapbox-routes/mapbox-routes.component";
import {MapboxRoutesEditComponent} from "../utils/directives/mapbox-routes-edit/mapbox-routes-edit.component";
import {IonSimpleWizard} from "../utils/directives/ion-simple-conditional-wizard/ion-simple-wizard.component";
import {IonSimpleWizardStep} from "../utils/directives/ion-simple-conditional-wizard/ion-simple-wizard.step.component";
import {DisableSubmitBtnDirective} from "../utils/directives/disable-submit-btn.directive";
import {DisableBtnDirective} from "../utils/directives/disable-btn.directive";

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginComponent,
    LogoutComponent,
    MyAccountComponent,
    EditAccountComponent,
    TransportationRequestsListComponent,
    TransportationRequestDetailsComponent,
    ChangePriceComponent,
    AssignEmployeeComponent,
    FinishTransportationComponent,
    TransportationRequestUpdateComponent,
    MapStepComponent,
    MapboxRoutesEditComponent,
    LoaderComponent,
    MapboxRoutesComponent,
    AddressInputModalComponent,
    IonSimpleWizard,
    IonSimpleWizardStep,
    DisableBtnDirective,
    DisableSubmitBtnDirective
  ],
  imports: [
    HTTPRequestsModule,
    NotificationsModule,
    ConfirmationModalModule,
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, { scrollAssist: false, autoFocusAssist: false, backButtonText: '', }),
    BrowserAnimationsModule,
    IonicStorageModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginComponent,
    LogoutComponent,
    MyAccountComponent,
    EditAccountComponent,
    TransportationRequestsListComponent,
    TransportationRequestDetailsComponent,
    ChangePriceComponent,
    AssignEmployeeComponent,
    FinishTransportationComponent,
    TransportationRequestUpdateComponent,
    MapStepComponent,
    AddressInputModalComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AccountService,
    TransportationRequestsService,
    WebsocketUtil,
    Geolocation,
    LaunchNavigator,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}
