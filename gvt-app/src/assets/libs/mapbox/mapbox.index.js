var mapboxgl = require('mapbox-gl');

if (module) {
    module.exports = mapboxgl;
}
window.mapboxgl = mapboxgl;
