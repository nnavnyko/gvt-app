/**
 *  Application Config
 *  Describe Constants of the project
    public static HOST = 'http://www.gruzi-v-taxi.by/';
 */

export class Config {
    public static HOST = 'https://www.gruzi-v-taxi.by';
    public static HOST_NO_PROTOCOL = 'www.gruzi-v-taxi.by';
    public static STATIC_URL = '/static/';
    public static APP_URL = '/static/gvt-app/app/';
    public static BIG_LOGO_IMG = 'assets/images/gvt_icon_big.png';
    public static LOGO_IMG_INVERSED = '/assets/images/gvt_icon_inversed.png';
    public static DEFAULT_AVATAR_IMG = 'assets/images/default-avatar.png';
    public static WEB_SOCKET_PREFIX = 'wss://';
    public static MAPBOX_STYLE = 'mapbox://styles/nnavnyko/cjpslwb1u07vc2rmhox79bm4h';

    public static getWSHost(): string {
        /**
         * Get current host
         *
         * @param authString: string like 'username:password'
         */
        return Config.WEB_SOCKET_PREFIX + Config.HOST_NO_PROTOCOL;
    }

    public static APP_VERSION = '0.0.7';
}
