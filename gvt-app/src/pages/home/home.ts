/**
 * Home Page Controller
 * Show common info, transportation statistics
 */
import {Component, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular';
import {TransportationRequestsService} from '../../transportation/shared/services/transportation-requests.service';
import {AccountService} from '../../accounts/shared/services/account.service';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';
import {MyAccountComponent} from "../../accounts/my-account/my-account.component";
import {TransportationRequestsListComponent} from "../../transportation/transportation-requests-list/transportation-requests-list.component";
import {TransportationRequestUpdateComponent} from "../../transportation/transportation-request-update/transportation-request-update.component";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  private totalRequests: number;
  private statisticsProgress: any;
  transportationRequestCreatePage: { title: string, component: any };
  myAccountPage: { title: string, component: any };
  transportationListPage: { title: string, component: any };

  constructor(public navCtrl: NavController, public transportationRequests: TransportationRequestsService,
              public account: AccountService, private translate: TranslateService) {

  }

  ngOnInit(): void {
    /**
     * Initialize component
     */
    this.myAccountPage = {title: 'Edit Account', component: MyAccountComponent};
    this.transportationListPage = {title: 'Transportation List', component: TransportationRequestsListComponent};
    this.transportationRequestCreatePage = {title: 'Create Order', component: TransportationRequestUpdateComponent};
  }

  openPage(page, params?: object) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.navCtrl.setRoot(page.component, params);
  }
}
